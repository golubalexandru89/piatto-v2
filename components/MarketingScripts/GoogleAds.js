'use client'

import { usePathname } from 'next/navigation'
import Script from 'next/script'
import { useEffect, useState } from 'react'
import { visitorDetailsContext } from "@/utils/contextProviders/VisitorDetailsContext"


const GoogleAdsPixel = () => {

    const [loaded, setLoaded] = useState(false)
    const pathname = usePathname()

    useEffect(() => {
        if (!loaded) return

        if (pathname.includes('meniu-local')) {

            window.gtag('event', 'Cod Scanat in Restaurant');
              
            window.gtag('event', 'conversion', {
                'send_to': 'AW-11077167726/BIXjCKPqguEYEO7UgKIp',
            });
        }

        window.gtag('event', 'page_view')
    }, [pathname, loaded])

    return (
        <>
            <Script async
                id="google-ads-pixel-js"
                strategy='afterInteractive'
                onLoad={() => setLoaded(true)}
                src="https://www.googletagmanager.com/gtag/js?id=AW-11077167726" />
            <Script
                id="google-ads-pixel"
                strategy='afterInteractive'
                onLoad={() => setLoaded(true)}
                dangerouslySetInnerHTML={{
                    __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config','G-1W5T8ZP7SJ')
                gtag('config', 'AW-11077167726');

            `}}
            />

        </>
    )
}

export default GoogleAdsPixel