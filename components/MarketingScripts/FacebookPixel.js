'use client'

import { usePathname } from 'next/navigation'
import Script from 'next/script'
import { useEffect, useState } from 'react'
import { visitorDetailsContext } from "@/utils/contextProviders/VisitorDetailsContext"



const pageview = (options) => {
  setTimeout(() => {
    window.fbq('track', 'PageView', options)
  }, "2000")
}

// https://developers.facebook.com/docs/facebook-pixel/advanced/
// Standard Event
const sEvent = (name, options = {}) => {
  window.fbq('track', name, options)
}
// Custom Event
const cEvent = (name, options = {}) => {

  window.fbq('trackCustom', name, options)
}


const FB_PIXEL_ID = "1137963430234171"
const FacebookPixel = () => {

  const { visitorDetails, visitorDetailsLoaded } = visitorDetailsContext()

  const [loaded, setLoaded] = useState(false)
  const pathname = usePathname()

  useEffect(() => {

    if (!loaded) return
    if (!visitorDetailsLoaded) return
    //console.log(visitorDetails)
    if (pathname.includes('meniu-local')) {
      sEvent('ViewContent')
      console.log(visitorDetails)
      window.fbq('trackCustom', 'Client Restaurant', JSON.stringify(visitorDetails))
      //cEvent('Client Restaurant', visitorDetails)
    }
    pageview(visitorDetails)

  }, [pathname, loaded, visitorDetailsLoaded])

  return (
    <div>
      <Script
        id="fb-pixel"
        src="/scripts/facebookPixel.js"
        strategy="afterInteractive"
        onLoad={() => setLoaded(true)}
        data-pixel-id={FB_PIXEL_ID}
      />
    </div>
  )
}

export default FacebookPixel