import Link from 'next/link';

function MobileFooter() {
    return (
        <div className="fixed bottom-0 float-none md:hidden backdrop-blur-lg  w-[90%] flex flex-row justify-evenly mb-5 px-3 py-1 text-center border-neutral-300	 border-2 rounded-full self-center text-sm">

            <div className="w-1/4 flex flex-col">
                <Link href="https://ialoc.ro/restaurante-bucuresti/piatto-77-rezervari-3999" target="_isblank">
                    <div>

                    </div>
                    <div>Rezervari</div>
                </Link>
            </div>
            <div className=" w-1/4 flex flex-col">
                <Link href='/meniu'>
                    <div></div>
                    <div>Meniu</div>
                </Link>
            </div>
            <div className="w-1/4 flex flex-col">
                <Link href="https://www.google.com/search?q=piatto+77#lrd=0x40b1f912527a8f5b:0x9d5babbaf9919549,1,,,," target="_isblank">
                    <div></div>
                    <div>Review-uri</div>
                </Link>
            </div>
            <div className=" w-1/4 flex flex-col">
                <Link href="https://goo.gl/maps/iy3SCNaRwmVn4eUM8" target="_isblank">
                    <div></div>
                    <div>Locație</div>
                </Link>
            </div>
        </div>

    )
}

export default MobileFooter;