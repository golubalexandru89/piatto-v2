import Link from "next/link";
import { useLanguageContext } from '@/utils/contextProviders/LanguageContext';

function Hero() {

    return (
        <>
            <div className="relative isolate p-6 md:px-4 lg:px-8">
                <div className="container mx-auto py-14 sm:py-48 lg:py-20 relative overflow-hidden border-2 rounded-xl">
                    <div className="bg-[url(/hero-image.jpg)] bg-center bg-cover h-full w-full absolute top-0 left-0"></div>
                    <div className="bg-black bg-opacity-10 backdrop-blur-sm	h-full w-full absolute top-0 left-0"></div>

                    <div className="text-left align-bottom md:w-2/3 w-full px-4 lg:px-20 md:pl-20 relative z-10">
                        <h1 className="md:text-[6rem] font-bold tracking-tight text-white text-[3.5rem] ">
                            <span className="text-transparent gradient-text animate-gradient">Urban Kitchen</span> by Piatto 77
                        </h1>
                        
                            <p className="mt-6 text-white text-xl">
                                O Abordare Modernă a Bucătăriei Urbane
                            </p>

                        <Link href="/meniu">
                            <button className="mt-8 bg-white rounded-full px-6 py-3 text-black">
                                Vezi Meniul
                            </button>
                        </Link>
                    </div>
                </div>
            </div>

        </>
    )
}

export default Hero;