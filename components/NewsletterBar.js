"use client"
import Link from 'next/link';
import { useState } from 'react';
import { useForm } from 'react-hook-form';


function NewsletterBar() {


    return (
        <>
            <div className=" px-1 md:px-4 py-14 lg:px-8 mx-auto container">
                <div className="container flex flex-col  justify-between align-middle text-center gap-4">

                    <div className="flex flex-col">
                        <h3 className="text-4xl">Află Meniul Zilnic</h3>
                    </div>


                    <div className="flex flex-col justify-center" >

                        <Link href="https://www.instagram.com/stories/piatto.77/?utm_source=piatto77.ro&utm_medium=referral&utm_campaign=We're awesome" target='_blank'>
                            <button size="lg" type="submit" className='bg-orange-400 px-5 py-3 md:py-2 rounded-full text-white'>Fii primul care află</button>
                        </Link>
                    </div>



                </div>
            </div>
        </>
    )
}
export default NewsletterBar;