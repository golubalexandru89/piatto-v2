import Card from "./Card";
import { useLanguageContext } from "../../utils/contextProviders/LanguageContext";




function CardList({ categoryName, items, setRecommendedItems,recommendedItems  }) {
    //console.log("CATEGORY NAME IS", categoryName)
    const { language } = useLanguageContext();
    
            
    
    return (
        <>
            <div className="flex flex-row overflow-x-scroll overflow-y-hidden gap-4 no-scrollbar">
                {items
                    .filter((item) => item.subCategory === categoryName)
                    .sort((a, b) => {
                        const order1 = parseFloat(a.visualOrderIndex);
                        const order2 = parseFloat(b.visualOrderIndex);
                        return order1 - order2;
                    })
                    .map((item) => (
                        
                        <Card item={item} key={item.id} id={item.id} language={language} className="z-1"></Card>
                    ))}
            </div>
        </>
    )
}
export default CardList
