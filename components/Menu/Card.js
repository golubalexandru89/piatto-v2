'use client'

import Image from "next/image";
import { AspectRatio } from "@/components/ui/AspectRatio"
import { InView, useInView } from 'react-intersection-observer'; // Import useInView
import { useState } from "react";
import { gtagEvent } from "@/utils/events"
import { insertBigQueryProductView } from "../../utils/bigQuery";

import { visitorDetailsContext } from "@/utils/contextProviders/VisitorDetailsContext"



function Card({ item, language }) {
    const { visitorDetails } = visitorDetailsContext()


    const [entryTimes, setEntryTimes] = useState({});

    const { ref, inView, entry } = useInView({
        /* Optional options */
        threshold: 0.75,
        root: null,
        rootMargin: '-300px -300px -300px -300px',
        initialInView: true
    });



    function intersectionObserverItem(itemName, inView, entry, item, ref) {

        //entry.target.style.backgroundColor = inView ? 'blue' : 'orange'


        // -> if needed, decomment the below code to ENABLE TIME PER VIEW PER PRODUCT
        // if (inView) {
        //     // Save the entry.time when inView is true
        //     setEntryTimes(prevTimes => ({
        //         ...prevTimes,
        //         [itemName]: entry.time
        //     }));
        // } else {
        //     // Calculate the difference when inView is false
        //     const savedTime = entryTimes[itemName];
        //     if (savedTime) {
        //         const timeDifference = entry.time - savedTime;
        //         gtagEvent("product_view", item, timeDifference)
        //         insertBigQueryProductView(item,timeDifference,visitorDetails);           
        //      }
        // }
    }





    return (
        <>
            {item.productFeatured === true ? (
                <div className="flex-none w-64 bg-gray-100 border-2 border-orange-400 rounded-lg shadow h-full min-h-full" id={item.id} sku={item.id} item-name={item.nameRo}>
                    <div className="min-h-[200px]">
                        {item.imageURL !== "" && (
                            <AspectRatio ratio={16 / 9} className="h-full">
                                <img
                                    className="rounded-t-lg object-cover w-full h-full min-h-[200px]"
                                    src={item.imageURL}
                                    alt={item.product}
                                />
                            </AspectRatio>
                        )}
                    </div>
                    <div className="flex flex-col justify-between p-5 h-fill">
                        <div className="" ref={ref}>
                            <InView key={item.id} onChange={(inView, entry) => intersectionObserverItem(item.nameRo, inView, entry, item, ref)} className="" id={item.id}>

                                {language == "ro" ? (
                                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{item.nameRo}</h5>
                                ) : (
                                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{item.nameEn}</h5>
                                )}
                            </InView>
                            {language === "ro" ? (
                                <p className="mb-3 font-normal text-gray-700 ">{item.descriptionRo}</p>
                            ) : (
                                <p className="mb-3 font-normal text-gray-700 ">{item.descriptionEn}</p>
                            )}
                        </div>
                        <div className="flex flex-row justify-between">
                            <div className="flex flex-col">
                                {language == 'ro' ? (
                                    <h5>Pret</h5>
                                ) : (
                                    <h5>Price</h5>

                                )}
                                <p>{item.price / 100}</p>
                            </div>
                            <div className="flex flex-col">
                                {language == 'ro' ? (
                                    <h5>Cant.</h5>
                                ) : (
                                    <h5>Quant.</h5>
                                )}
                                <p>{item.weight} {item.weightUnit}</p>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <div className="flex-none w-64 bg-gray-100 border border-gray-200 rounded-lg shadow h-full min-h-full" id={item.id} sku={item.id} item-name={item.nameRo}>
                    <div className="min-h-[200px]">
                        {item.imageURL === "" ? null : (
                            <AspectRatio ratio={16 / 9} className="h-full">
                                <img
                                    className="rounded-t-lg object-cover w-full h-full min-h-[200px]"
                                    src={item.imageURL}
                                    alt={item.product}
                                />
                            </AspectRatio>
                        )}
                    </div>
                    <div className="flex flex-col justify-between p-5 h-fill">
                        <div className="" ref={ref}>
                            <InView key={item.id} onChange={(inView, entry) => intersectionObserverItem(item.nameRo, inView, entry, item, ref)} className="" id={item.id}>

                                {language == "ro" ? (
                                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{item.nameRo}</h5>
                                ) : (
                                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{item.nameEn}</h5>
                                )}
                            </InView>
                            {language === "ro" ? (
                                <p className="mb-3 font-normal text-gray-700 ">{item.descriptionRo}</p>
                            ) : (
                                <p className="mb-3 font-normal text-gray-700 ">{item.descriptionEn}</p>
                            )}
                        </div>
                        <div className="flex flex-row justify-between">
                            <div className="flex flex-col">
                                {language == 'ro' ? (
                                    <h5>Pret</h5>
                                ) : (
                                    <h5>Price</h5>

                                )}
                                <p>{item.price / 100}</p>
                            </div>
                            <div className="flex flex-col">
                                {language == 'ro' ? (
                                    <h5>Cant.</h5>
                                ) : (
                                    <h5>Quant.</h5>
                                )}
                                <p>{item.weight} {item.weightUnit}</p>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}


export default Card;