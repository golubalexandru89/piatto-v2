function Logo() {
    return (
        <>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 328.06 82.66" width="150px" height="100%">
                                <g id="Layer_2" data-name="Layer 2">
                                    <g id="Layer_1-2" data-name="Layer 1">
                                        <g>
                                            <g id="Piatto">
                                                <path d="M250.19,56.09,273.54,8.37H249V0h28.13c3.94,0,6.37,2.51,6.37,5.86a9.44,9.44,0,0,1-.93,3.68L259.9,56.09Z" style={{fill: "#4f7f7b"}} />
                                                <path d="M294.74,56.24,318.1,8.52H293.57V.15h28.12c3.94,0,6.37,2.52,6.37,5.86a9.64,9.64,0,0,1-.92,3.69L304.45,56.24Z" style={{fill: "#4f7f7b"}} />
                                                <rect x="35.64" y="2.3" width="7.96" height="51.8" style={{fill: "#4f7f7b"}} />
                                                <path d="M8.32,31.73V54.1H0V5.73C0,2.18,2.26,0,5.81,0h4.93c5,0,9.12,1.45,12.27,4.36s4.77,6.7,4.77,11.55S26.16,24.63,23,27.45s-7.18,4.28-12.27,4.28Zm0-8.07H10.5c5.25,0,8.8-2.91,8.8-7.75s-3.64-7.84-8.8-7.84H8.32Z" style={{fill: "#4f7f7b"}} />
                                                <polygon points="109.86 8.07 98.88 8.07 98.88 0 129.24 0 129.24 8.07 118.26 8.07 118.26 54.1 109.86 54.1 109.86 8.07" style={{fill: "#4f7f7b"}} />
                                                <path d="M86.12,5.85C82.83,1.92,78,0,71.85,0S61,1.92,57.66,5.85s-5,9.06-5,15.39V54.1H61V21.24C61,12.34,65.52,8,71.85,8s11,4.33,11,13.23V54.1h8.25V21.24c0-6.33-1.68-11.46-5-15.39" style={{fill: "#4f7f7b"}} />
                                                <path d="M71.89,16.62a6.57,6.57,0,1,0,6.57,6.56,6.57,6.57,0,0,0-6.57-6.56" style={{fill: "#dc5e30"}} />
                                                <path d="M219.25,8.51A26.56,26.56,0,0,0,199.56.31a26.87,26.87,0,0,0-19.77,8.2,26.83,26.83,0,0,0-8.12,19.69A27.15,27.15,0,0,0,179.79,48a27.15,27.15,0,0,0,19.77,8.12A26.83,26.83,0,0,0,219.25,48a26.87,26.87,0,0,0,8.2-19.77,26.56,26.56,0,0,0-8.2-19.69m-6.07,33.63a18.27,18.27,0,0,1-13.62,5.75,18.6,18.6,0,0,1-13.7-5.75A19.26,19.26,0,0,1,180.2,28.2a19.1,19.1,0,0,1,5.66-13.87,18.7,18.7,0,0,1,13.7-5.82,18.37,18.37,0,0,1,13.62,5.82,18.82,18.82,0,0,1,5.74,13.87,19,19,0,0,1-5.74,13.94" style={{fill: "#4f7f7b"}} />
                                                <path d="M199.56,21.48a6.72,6.72,0,1,0,6.72,6.72,6.72,6.72,0,0,0-6.72-6.72" style={{fill: "#dc5e30"}} />
                                                <polygon points="149.5 8.07 138.52 8.07 138.52 0 168.88 0 168.88 8.07 157.9 8.07 157.9 54.1 149.5 54.1 149.5 8.07" style={{fill: "#4f7f7b"}} />
                                            </g>
                                            <g id="Urban_Kitchen" data-name="Urban Kitchen">
                                                <path d="M154.26,76.29a2.57,2.57,0,0,0,.44,1.41,3.52,3.52,0,0,0,1.17,1.1,3.16,3.16,0,0,0,1.6.42,3.31,3.31,0,0,0,2.85-1.52,2.56,2.56,0,0,0,.43-1.41V66.51h3v9.84a5.52,5.52,0,0,1-.84,3,6,6,0,0,1-2.28,2.07,6.77,6.77,0,0,1-3.18.74,6.66,6.66,0,0,1-3.16-.74,5.86,5.86,0,0,1-2.26-2.07,5.45,5.45,0,0,1-.85-3V66.51h3.06Z" style={{fill: "#dc5e30"}} />
                                                <path d="M172.16,66.51a8.26,8.26,0,0,1,2.59.37,5.11,5.11,0,0,1,1.91,1.05,4.37,4.37,0,0,1,1.17,1.65,5.44,5.44,0,0,1,.4,2.16,6.51,6.51,0,0,1-.28,1.89,4.84,4.84,0,0,1-.93,1.73,4.61,4.61,0,0,1-1.7,1.26,6.45,6.45,0,0,1-2.61.47h-2.19V82h-3.06V66.51Zm.53,7.63a2.66,2.66,0,0,0,1.16-.23,1.94,1.94,0,0,0,.72-.59,2.22,2.22,0,0,0,.38-.75,2.47,2.47,0,0,0,.12-.73,3.43,3.43,0,0,0-.07-.64,2.45,2.45,0,0,0-.31-.79,1.87,1.87,0,0,0-.71-.68,2.55,2.55,0,0,0-1.27-.27h-2.19v4.68Zm2.87,1.9,3.83,6h-3.56l-3.92-5.91Z" style={{fill: "#dc5e30"}} />
                                                <path d="M186.48,66.51a6.71,6.71,0,0,1,3.86,1,3.31,3.31,0,0,1,1.43,2.92,3.94,3.94,0,0,1-.71,2.39,4.26,4.26,0,0,1-1.92,1.44,7.66,7.66,0,0,1-2.77.47l-.48-1.64a10.16,10.16,0,0,1,3.4.52,5.5,5.5,0,0,1,2.35,1.51,3.42,3.42,0,0,1,.87,2.35,4.51,4.51,0,0,1-.46,2.14A3.88,3.88,0,0,1,190.83,81a5.1,5.1,0,0,1-1.74.78,7.77,7.77,0,0,1-2,.25h-5.17V66.51Zm.36,6.34a1.59,1.59,0,0,0,1.33-.54,2,2,0,0,0,.44-1.25,1.41,1.41,0,0,0-.54-1.2,2.34,2.34,0,0,0-1.44-.4H185v3.39Zm.1,6.33a3.86,3.86,0,0,0,1.22-.18,1.8,1.8,0,0,0,.87-.57,1.52,1.52,0,0,0,.32-1,1.32,1.32,0,0,0-.38-1,2.27,2.27,0,0,0-1-.49,5.79,5.79,0,0,0-1.26-.13H185v3.37Z" style={{fill: "#dc5e30"}} />
                                                <path d="M193.29,82l7-16.13h.17l7,16.13h-3.55l-4.47-11.35,2.22-1.52L196.3,82Zm4.78-5.61h4.66L203.81,79h-6.69Z" style={{fill: "#dc5e30"}} />
                                                <polygon points="223.61 82.66 211.84 72.03 212.75 72.54 212.81 82.03 209.71 82.03 209.71 65.9 209.84 65.9 221.35 76.48 220.68 76.19 220.61 66.51 223.69 66.51 223.69 82.66 223.61 82.66" style={{fill: "#dc5e30"}} />
                                                <polygon points="237.65 72.03 237.56 72.81 238.03 72.22 243.09 66.51 247.05 66.51 240.64 73.47 247.07 82.03 243.19 82.03 238.41 75.41 237.65 76.16 237.65 82.03 234.55 82.03 234.55 66.51 237.65 66.51 237.65 72.03" style={{fill: "#dc5e30"}} />
                                                <rect x="249.39" y="66.51" width="3.06" height="15.52" style={{fill: "#dc5e30"}} />
                                                <polygon points="255.4 66.51 265.86 66.51 265.86 69.46 262.09 69.46 262.09 82.03 259.03 82.03 259.03 69.46 255.4 69.46 255.4 66.51" style={{fill: "#dc5e30"}} />
                                                <path d="M279.92,80.74a6.25,6.25,0,0,1-.93.57,8.47,8.47,0,0,1-1.68.63,7.58,7.58,0,0,1-2.19.25,8.63,8.63,0,0,1-3.28-.66,7,7,0,0,1-2.45-1.7,7.38,7.38,0,0,1-1.53-2.46,8.37,8.37,0,0,1-.52-3,9.33,9.33,0,0,1,.53-3.24,7.58,7.58,0,0,1,1.54-2.56,7,7,0,0,1,2.4-1.66,7.82,7.82,0,0,1,3.07-.59,8.44,8.44,0,0,1,2.77.42,9.26,9.26,0,0,1,2,.91l-1.2,2.89a8.87,8.87,0,0,0-1.41-.84,4.44,4.44,0,0,0-2-.43,4,4,0,0,0-1.7.37,4.42,4.42,0,0,0-1.43,1,5.08,5.08,0,0,0-1,1.57,5.2,5.2,0,0,0-.36,2,6.08,6.08,0,0,0,.33,2,4.48,4.48,0,0,0,.93,1.59,4.1,4.1,0,0,0,1.47,1,4.92,4.92,0,0,0,1.93.36A5.1,5.1,0,0,0,278.66,78Z" style={{fill: "#dc5e30"}} />
                                                <polygon points="296.03 66.51 296.03 82.03 292.98 82.03 292.98 75.72 286.04 75.72 286.04 82.03 282.98 82.03 282.98 66.51 286.04 66.51 286.04 72.77 292.98 72.77 292.98 66.51 296.03 66.51" style={{fill: "#dc5e30"}} />
                                                <polygon points="300.04 66.51 310.62 66.51 310.62 69.46 303.1 69.46 303.1 72.77 309.76 72.77 309.76 75.72 303.1 75.72 303.1 79.08 310.92 79.08 310.92 82.03 300.04 82.03 300.04 66.51" style={{fill: "#dc5e30"}} />
                                                <polygon points="327.97 82.66 316.21 72.03 317.12 72.54 317.18 82.03 314.08 82.03 314.08 65.9 314.21 65.9 325.72 76.48 325.04 76.19 324.98 66.51 328.06 66.51 328.06 82.66 327.97 82.66" style={{fill: "#dc5e30"}} />
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
        </>
    )
}

export default Logo;