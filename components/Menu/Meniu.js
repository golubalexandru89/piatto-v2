'use client'
import CategoryMenu from "./Category";
import Link from "next/link";
import { useState } from "react";
import ImagesStories from "./Stories";


function Meniu({ meniu }) {


  const [recommendedItems, setRecommendedItems] = useState([]);

  function adjustRecommendedItems(item, entry, inView) {
    let itemIndex = recommendedItems.findIndex(obj => obj.id === item.id)
    if (itemIndex === -1) {
      setRecommendedItems(prevItems => [
        ...prevItems,
        { ...item, inViewAt: inView ? entry.time : null, outViewAt: inView ? null : entry.time, viewTime: 0 }
      ])
      // recommend.push({ ...item, inViewAt: inView ? entry.time : null, outViewAt: inView ? null : entry.time, viewTime: 0 })
    }
    else {

      if (inView) {
        setRecommendedItems(prevItems => {
          const updatedItems = [...prevItems];
          updatedItems[itemIndex].inViewAt = entry.time;
          return updatedItems;
        })
      }
      else if (!inView) {
        setRecommendedItems(prevItems => {
          const updatedItems = [...prevItems];
          updatedItems[itemIndex].outViewAt = entry.time;
          updatedItems[itemIndex].viewTime += updatedItems[itemIndex].outViewAt - updatedItems[itemIndex].inViewAt;
          return updatedItems;
        })
      }

    }
  }

  const Meniu = meniu
  const meniuSorted = Meniu.sort((a, b) => a.categoryOrder - b.categoryOrder)

  const meniuCategorySet = new Set(meniuSorted.map((item) => item.subCategory))
  const meniuCategoryArray = Array.from(meniuCategorySet)




  return (
    <>

      <div className="pt-2 pb-20">

        {/* <RecommendationEngine recommendedItems={recommendedItems}></RecommendationEngine>*/}
        {/* <ImagesStories /> */}
        <CategoryMenu category={meniuCategoryArray} items={meniuSorted} setRecommendedItems={adjustRecommendedItems} recommendedItems={recommendedItems} id="menu-component"></CategoryMenu>


      </div>
      <div className="container  mx-auto py-10 text-center">
        <Link className="" href="/Meniu-Piatto-77-Valori-Nutritionale.pdf" target="_blank">Pentru valori nutriționale, apăsați aici.</Link>
      </div>
    </>
  );
}
export default Meniu