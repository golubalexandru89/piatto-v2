'use client'

import { useEffect, useState } from "react"
import Card from "./Card";
import { useLanguageContext } from "../../utils/contextProviders/LanguageContext";

export default function RecommendationEngine({ recommendedItems }) {
    const { language } = useLanguageContext();
    const [renderHtml, setRenderHtml] = useState(false);

    const sortedItems = recommendedItems.slice().sort((a, b) => b.viewTime - a.viewTime).slice(0, 5);

    useEffect(() => {
        const timeoutId = setTimeout(() => {
            setRenderHtml(true);
        }, 15000); // 15 seconds timeout

        return () => clearTimeout(timeoutId); // Cleanup the timeout on component unmount

    }, [recommendedItems]);

    return (
        <>
            {renderHtml && (
                <div className="pt-5 px-2">
                    <h3 className="text-2xl pb-4 font-semibold text-orange-600"> Recomandat pentru Tine</h3>
                    <div className="flex flex-row overflow-x-scroll overflow-y-hidden gap-4 no-scrollbar">
                        {sortedItems.map((item, index) => (
                            <span key={index}>
                                <Card item={item} key={item.id} id={item.id} language={language} className="z-1"></Card>
                            </span>
                        ))}
                    </div>
                </div>
            )}
        </>
    )
}
