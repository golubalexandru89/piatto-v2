import { useEffect } from "react";
import { useLanguageContext } from "../../utils/contextProviders/LanguageContext";
import CardList from "./CardList";
import { InView, useInView } from 'react-intersection-observer'; // Import useInView




export default function CategoryMenu({ category, items, setRecommendedItems,recommendedItems }) {
  
  const { language } = useLanguageContext();

  

    const { ref, inView, entry } = useInView({
        /* Optional options */
        threshold: 1,
        rootMargin: '30px 0px 150px 0px',
        initialInView: true
    });

    let menuLoaded = false
    let load_time = null;
    let last_time = null;
    let this_time = null;
    let this_section_name = null;
    let new_section_name = null;

  return (
    <>
      <div>
        {category.map((categoryObject) => (
          <div className="pt-5 px-2" key={categoryObject} id={categoryObject}>
                <h3 className="text-2xl pb-4 font-semibold text-orange-600">{categoryObject}
                  {categoryObject == "Morning Breakfast" && <span className="text-base pb-4 font-normal"> | L-V: 10-11.30</span>}
                  {categoryObject == 'Weekend Brunch' && <span className="text-base pb-4 font-normal"> | S-D: 11-16</span>}
                  {categoryObject == "Wood Fired Pizza" && <span className="text-base pb-4 font-normal"> | 32 CM</span>}
                </h3>
                <CardList categoryName={categoryObject} items={items} setRecommendedItems={setRecommendedItems} recommendedItems={recommendedItems}></CardList>
          </div>
        ))}
      </div>
    </>
  );

}