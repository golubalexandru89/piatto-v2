'use client'
import * as React from "react"
import Autoplay from "embla-carousel-autoplay"

import {
    Carousel,
    CarouselContent,
    CarouselItem,

} from "@/components/ui/carousel"
import Image from "next/image"
import { InView, useInView } from 'react-intersection-observer'; // Import useInView
import { useState } from "react";
import { promotionViewEvent } from "@/utils/events"
import { insertBigQueryPromotionView } from "../../utils/bigQuery"
import { visitorDetailsContext } from "@/utils/contextProviders/VisitorDetailsContext"


export default function ImagesStories() {
    const { visitorDetails } = visitorDetailsContext()

    const plugin = React.useRef(
        Autoplay({ delay: 1500, stopOnInteraction: true, loop: true })
    )

    const promos = [
        {
            type: 'image',
            url: '/promotions/disarono-velvet.jpg',
            duration: 2500,
            promotion: "Disaronno Velvet - Cover"
        },
        {
            type: 'image',
            url: '/promotions/disarono-velvet-price.jpg',
            duration: 2500,
            promotion: "Disaronno Velvet - Price List"

        },
        {
            type: 'image',
            url: '/promotions/monin-price.jpg',
            duration: 2500,
            promotion: "Monin - Price List"

        },
    ];

    const [entryTimes, setEntryTimes] = useState({});

    const { ref, inView, entry } = useInView({
        /* Optional options */
        threshold: 0.75,
        root: null,
        rootMargin: '-300px -300px -300px -300px',
        initialInView: true
    });



    function intersectionObserverItem(itemName, inView, entry, item, ref) {

        //entry.target.style.backgroundColor = inView ? 'blue' : 'orange'

        if (inView) {
            // Save the entry.time when inView is true
            setEntryTimes(prevTimes => ({
                ...prevTimes,
                [itemName]: entry.time
            }));
        } else {
            // Calculate the difference when inView is false
            const savedTime = entryTimes[itemName];
            if (savedTime) {
                const timeDifference = entry.time - savedTime;
                //console.log(timeDifference, item)
                if (timeDifference.toFixed(0) > 750) {
                    promotionViewEvent("promotion_view", item, timeDifference)
                    insertBigQueryPromotionView(item, timeDifference, visitorDetails)
                }

            }
        }
    }

    return (
        <>
            <div className="p-1 h-[70vh]">
                <Carousel
                    plugins={[plugin.current]}
                    className="h-[70vh]  max-w-xs mx-auto"
                    onMouseEnter={plugin.current.stop}
                    onMouseLeave={plugin.current.reset}
                >

                    <CarouselContent className="h-[70vh]">
                        {promos.map((i) => (
                            <CarouselItem key={i.url} className="h-full">
                                <div className="p-1 h-[100%] min-h-full">

                                    <div className="relative h-full w-full">
                                        <InView key={i.promotion} onChange={(inView, entry) => intersectionObserverItem(i.promotion, inView, entry, i, ref)} className="" id={i.url}>
                                            <Image src={i.url} fill={true} style={{ objectFit: "contain" }} alt="promotional banner"></Image>
                                        </InView>
                                    </div>

                                </div>
                            </CarouselItem>
                        ))}
                    </CarouselContent>

                </Carousel>
            </div>
        </>
    )
}
