import { Button } from "@/components/ui/button"
import Link from "next/link";
function CallToActionButtons() {
    return (
        <>
            <div className="flex flex-row justify-evenly gap-4">
                <Link href='https://piatto77.ro/evenimente'>
                    <Button className="">
                        Petreceri Private
                    </Button>
                </Link>
                <Link href='https://ialoc.ro/restaurante-bucuresti/piatto-77-rezervari-3999'>
                    <Button className="">
                        Rezervări
                    </Button>
                </Link>


            </div>
        </>
    )
}
export default CallToActionButtons;