'use client'
import { useEffect, useState, useRef } from 'react';
import { toast } from 'react-hot-toast';

function AnnouncementToaster() {
    const [read, setRead] = useState(false);
    const isEffectTriggered = useRef(false);

    function onClose() {
        setRead(true);
        toast.dismiss();
    }

    useEffect(() => {
        // Display the toast only on the client side after the initial render
        if (!isEffectTriggered.current && !read) {
            isEffectTriggered.current = true; // Mark the effect as triggered
            toast.custom(
                <div className='bg-red-600 w-1/3 h-2/5 min-w-[450px] min-h-[400px] rounded-md border-gray-400 borderd-2 relative flex flex-col justify-center'>
                  {/* Background image with opacity */}
                  <div
                    className="absolute inset-0 bg-[url(/snow-background.png)] bg-no-repeat bg-cover rounded-md"
                    style={{ opacity: 0.25 }}
                  ></div>
              
                  <div className="relative p-8 leading-relaxed gap-1 text-white z-10 flex flex-col justify-evenly">
                    {/* Main content */}
                    <h3 className='text-xl font-semibold'>🥳 Program Sărbători 🎄🎅</h3>
                    <p className='mt-2'>În perioada 24-26 Decembrie și 31 Decembrie-1 Ianuarie, restaurantul este închis.</p>
                    <p className='mt-2'>Îți mulțumim că ai fost alături de noi și te așteptăm cu drag în perioada 27-30 Decembrie. </p>
                    <p className='mt-2'>Iar dacă nu poți ajunge până la final de an, te invităm la un mic dejun English Monster în data de 2 Ianuarie, să combatem mahmureala împreună.</p>
              
                    {/* Button */}
                    <button className="mt-2 px-4 py-2 rounded-md border border-x-slate-100 bg-white text-black z-20" onClick={() => onClose()}>Închide</button>
                  </div>
                </div>,
                {
                    duration: 25000,
                    style: {
                        minWidth: '250px',
                        minHeight: '200px'
                    }
                });
        }
    }, [read]);

    return <></>;
}

export default AnnouncementToaster;