'use client'
import Link from "next/link";
import { useLanguageContext } from "../utils/contextProviders/LanguageContext";
import Logo from "./Menu/Logo";
import Script from "next/script";
import SocialIcons from "./SocialIcons";
import CallToActionButtons from "./CallToActionButtons"

function Header() {
    const { language, toggleLanguage } = useLanguageContext();
    return (
        <>
            <div className="  w-full md:w-1/2 container mx-auto">
                <SocialIcons></SocialIcons>
            </div>
            <nav className="bg-white border-gray-200 ">
                <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl p-4">
                    <a href="https://piatto77.ro" className="flex items-center space-x-3 rtl:space-x-reverse">
                        <div>
                            <Logo></Logo>
                        </div>
                    </a>
                    <div id="CTA">
                        <CallToActionButtons/>
                    </div>
                    <div className="flex items-center space-x-6 rtl:space-x-reverse">
                        {language == 'ro' ? (
                            <button type="button" onClick={() => toggleLanguage()} className="inline-flex items-center font-medium justify-center px-4 py-2 text-sm text-gray-400 rounder-lg cursor-pointer">
                                English
                            </button>
                        ) : (
                            <button type="button" onClick={() => toggleLanguage()} className="inline-flex items-center font-medium justify-center px-4 py-2 text-sm text-gray-400 rounder-lg cursor-pointer">
                                Română
                            </button>
                        )}

                    </div>
                </div>
            </nav>

            <nav className="bg-gray-50 sticky top-0 bg-gradient-to-r from-gray-50 via-slate-50 to-zinc-50 background-animate animate-pulse	z-20	">
                <div className="max-w-screen-xl px-4 py-3 mx-auto">
                    <div className="flex items-center align-middle justify-center">
                        <ul className="flex flex-row font-medium mt-0 space-x-8 rtl:space-x-reverse text-sm">
                            <li>
                                {language == 'ro' ? (
                                    <Link href="/" className="text-gray-900 hover:underline" aria-current="page">Acasă</Link>
                                ) : (
                                    <Link href="/" className="text-gray-900 hover:underline" aria-current="page">Home</Link>
                                )}
                            </li>
                            <li>
                                <Link href="/urban-kitchen" className="text-gray-900 hover:underline" aria-current="page">Urban Kitchen</Link>
                            </li>
                            <li>
                                {language == 'ro' ? (
                                    <Link href="/meniu" className="text-gray-900 hover:underline" aria-current="page">
                                        Meniu
                                    </Link>
                                ) : (
                                    <Link href="/meniu" className="text-gray-900 hover:underline" aria-current="page">
                                        Menu
                                    </Link>
                                )}
                            </li>
                            <li>
                                <Link href="/contact" className="text-gray-900 hover:underline" aria-current="page">Contact</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav >
        </>
    )
}
export default Header;