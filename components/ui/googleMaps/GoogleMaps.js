function GoogleMapsLocation() {
    return (
        <>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d891.9524842960427!2d26.101877417004346!3d44.45004895489803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1f912527a8f5b%3A0x9d5babbaf9919549!2sPIATTO%2077!5e0!3m2!1sen!2sro!4v1701124230224!5m2!1sen!2sro"
                width="100%"
                height="400px"
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade">
            </iframe>
        </>
    )
}

export default GoogleMapsLocation;