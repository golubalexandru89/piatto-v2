'use client'

import { useEffect } from "react"
import { visitorDetailsContext } from "../utils/contextProviders/VisitorDetailsContext"
import { useSearchParams, usePathname } from 'next/navigation'
import CreateUserId from "../utils/userDetailsFunctions/createUserId"
import GetTimeStamp from "../utils/userDetailsFunctions/getTimestamp"
import { encryptString, decryptString } from "../utils/crypto"
import { insertBigQueryPageView } from "../utils/bigQuery.js"


// const fingerprintjs = import("../utils/userDetailsFunctions/fingerprint.js")
//     .then(FingerprintJS => FingerprintJS.load())

// const thumbmark = import('../utils/userDetailsFunctions/thumbmark.js').then(Thumbmark => Thumbmark.getFingerprint())


const checkSession = async (userDetails, urlParams) => {
    // console.log("20. CheckSession Started", userDetails, urlParams)
    //GRAB USER SESSION LIST
    const sess = userDetails.sessions

    //FIND OUT TODAY 
    const { date, hour } = GetTimeStamp();

    //FIND OUT THE LAST SESSION IN THE LIST. 
    const latestSession = sess.length - 1;
    const s = sess[latestSession]

    //IF THE USER'S LAST SESSION IS EQUAL TO TODAY (AKA. IT'S ANOTHER SESSION IN THE SAME DAY)
    if (s.date == date) {

        // AND IF THE USER'S LAST SESSION IS WITHIN +/- 2 HOURS FROM THE LAST ONE

        const sessionHour = parseInt(s.time.substring(0, 2)) + 2
        const currentHour = parseInt(hour.substring(0, 2))

        if ((currentHour >= sessionHour && currentHour <= sessionHour + 2) ||
            (currentHour <= sessionHour && currentHour >= sessionHour - 2)) {
            //DO NOTHING

        } else {

            // IF THIS SESSION IS OUTSIDE OF +/- 2 HOURS FROM LAST ONE, ADD A NEW SESSION TO LIST
            sess.push({
                "session_number": sess.length + 1,
                "date": date,
                "time": hour,
                "traffic_details": urlParams
            })
        }
        //IF TODAY AND THE LAST SESSION DAY ARE NOT THE SAME, THEN IT'S A NEW SESSION FOR SURE
    } else if (s.date !== date) {
        //ADD THIS NEW SESSION TO LIST
        sess.push({
            "session_number": sess.length + 1,
            "date": date,
            "time": hour,
            "traffic_details": urlParams
        })

        //ALSO, UPDATE THE DATE OF THE LAST SESSION FOR NOT THIS ONE BUT THE LAST ONE
        userDetails.last_session_time = sess[sess.length - 2].date
        // console.log("21. Append userDetails Last Session Time", userDetails.last_session_time)
    } else {
        console.log("Somehow this error is unexpected. Maybe we should investigate it before it starts acting funny.")
    }
}
const checkVisit = async (userDetails) => {
    // console.log("24. Check Visit Started", userDetails)
    // GRAB USER VISIT LIST
    const visit = userDetails.visits || [];

    // FIND OUT TODAY 
    const { date, hour } = GetTimeStamp();

    // FIND OUT THE LAST VISIT IN THE LIST. 
    const latestVisit = visit.length > 0 ? visit.length - 1 : 0;
    const lastVisit = visit.length > 0 ? visit[latestVisit] : null;

    // IF THE USER'S LAST VISIT IS EQUAL TO TODAY (AKA. IT'S ANOTHER VISIT IN THE SAME DAY)
    if (lastVisit && lastVisit.date === date) {
        const visitHour = lastVisit.time ? parseInt(lastVisit.time.substring(0, 2)) : parseInt(hour);
        const currentHour = parseInt(hour.substring(0, 2));

        // AND IF THE USER'S LAST VISIT IS WITHIN +/- 2 HOURS FROM THE LAST ONE
        if ((currentHour >= visitHour && currentHour <= visitHour + 2) ||
            (currentHour <= visitHour && currentHour >= visitHour - 2)) {
            // DO NOTHING
        } else {
            // IF THIS VISIT IS OUTSIDE OF +/- 2 HOURS FROM LAST ONE, ADD A NEW VISIT TO THE LIST
            userDetails.visits = [...visit, {
                "visit_number": visit.length + 1,
                "date": date,
                "time": hour,
            }];
        }
    } else {
        // ADD THIS NEW VISIT TO LIST
        userDetails.visits = [...visit, {
            "visit_number": visit.length + 1,
            "date": date,
            "time": hour,
        }];
    }
    // console.log('25. Update the Two Variables about visit history')
    // ALSO, UPDATE THE TWO VARIABLES ABOUT VISIT HISTORY
    userDetails.total_number_of_visits = userDetails.visits.length;
    userDetails.last_visit_time = userDetails.visits.length > 1 ? userDetails.visits[userDetails.visits.length - 2].date : null;
}
const dateDiff = async (oldDate) => {
    //A FUNCTION TO CALCULATE THE DIFFERENCE BETWEEN TODAY AND AN OLDER DATE
    const dateNow = new Date();
    const day = dateNow.getDate()
    const month = dateNow.getMonth() + 1

    const old = oldDate.split("-")

    let daysDifference = day - Number(old[0]) + (month - Number(old[1])) * 30.4;

    return daysDifference
}
const firstTimeUser = async (urlParams, pathname) => {
    // console.log("7. FIRST TIME USER FUNCTION STARTED")
    //THIS FUNCTION IS CALLED WHEN THERE IS NO USER AND WE NEED TO ADD DATA ABOUT TODAY'S VISIT

    //CREATE AN OBJECT
    let metaData = {};

    //CREATE Uniqu User ID (UUID)
    const UUID = CreateUserId();

    //APPEND UUID 
    metaData.puuid = UUID;

    //GET TIMESTAMP
    const { date, hour } = GetTimeStamp();

    //SET FIRST SOURCE (TRAFFIC) DETAILS
    metaData.first = urlParams;
    metaData.first.date = date;
    metaData.first.hour = hour;

    //SET LAST SESSION FOR TODAY, CAUSE IT'S THE USER'S FIRST VISIT
    metaData.last_session_time = date;
    metaData.days_since_last_session = 0;

    //IF THE USER'S FIRST VISIT IS A SCAN OF THE MENU, WE ADD THESE DETAILS ALSO
    if (pathname.includes('meniu-local')) {
        metaData.last_visit_time = date;
        metaData.days_since_last_visit = 0;
        metaData.total_number_of_visits = 1;
        metaData.visits = []
        metaData.visits.push({
            "visit_number": 1,
            "date": date,
            "time": hour
        })
    }


    metaData.sessions = []
    metaData.sessions.push({
        "session_number": 1,
        "date": date,
        "time": hour,
        "traffic_details": urlParams
    })

    // console.log("8. Metadata is", metaData)
    const encrypted = await encryptString(JSON.stringify(metaData))
    // console.log("9. Encrypted string is",encrypted)

    window.localStorage.setItem('journey', encrypted)
    // console.log("10. Pushed to Local Storage", encrypted)

}
const getUserDetails = async () => {
    try {
        // console.log("4. GetUserDetails Started")
        // CHEC IF IT'S POSSIBLE TO FIND A WINDOW AND IF IT HAS LOCALSTORAGE
        if (typeof window !== 'undefined' && window.localStorage) {
            // console.log("5. If there is a window and localstorage")
            //SAVE INTO USERDETAILSTRING THE LOCALSTORAGE ITEM CALLED 'JOURNEY'
            const userDetailsString = localStorage.getItem('journey');
            // console.log("6. UserDetailsString string", userDetailsString)
            //IF THE LOCALSTORAGE DOESN'T FIND 'JOURNEY' THEN RETURN NULL IN USEEFFECT FUNCTION
            if (userDetailsString === null) {
                // console.log("7. If there is no localStorage, then return null")
                return userDetailsString;
            }

            //ELSE, PARSE THROUGHT THE JSON AND RETURN IT TO USEEFFECT FUNCTION
            //const unDecryptedUserDetails = await decryptString(userDetailsString)
            try {
                const userDetails = JSON.parse(userDetailsString)
                return userDetails
            }
            catch (err) {
                const unDecoded = await decryptString(userDetailsString)
                // console.log("8. Decrypt localStorage", unDecoded)
                const userDetails = JSON.parse(unDecoded);
                // console.log("9. Parse JSON", userDetails)
                return userDetails
            }

        } else {
            console.warn('localStorage is not available in this environment.')
            return null;
        }

    }
    catch (error) {
        console.error('Error while getting user details from localStorage: ', error)
    }
}
const appendDetails = async (userDetails, urlParams, pathname) => {
    //APPEND DETAILS, HAS SEPARATE FUNCTIONS OF ADDING TO SESSIONS AND VISITS
    // console.log("19. Append Details Startd", userDetails, urlParams, pathname)
    //CHECKSESSION WILL ADD ANOTER DETAIL TO SESSIONS
    checkSession(userDetails, urlParams);

    //THIS UPDATES THE DAYS_SINCE_LAST_SESSION
    // console.log("22. Last Session & Days since last session logic")
    const lastSession = userDetails.sessions.length - 2
    if (userDetails.sessions.length > 1 && userDetails.sessions[lastSession]) {
        userDetails.days_since_last_session = dateDiff(userDetails.sessions[lastSession].date);
    } else {
        // Handle the case where there are no sessions or not enough sessions
        // to calculate days_since_last_session.
        userDetails.days_since_last_session = 0; // Set a default value or handle it accordingly.
    }

    //BUT IF THE USER IS INSIDE THE RESTAURANT, WE ADD THE VISIT ALSO
    if (pathname.includes('meniu-local')) {
        // console.log("23. If Meniu Local")
        checkVisit(userDetails)

        //THIS UPDATES THE DAYS_SINCE_LAST_VISIT VARIABLE
        const lastVisitIndex = userDetails.visits.length > 1 ? userDetails.visits.length - 2 : 0;
        userDetails.days_since_last_visit = dateDiff(userDetails.visits[lastVisitIndex]?.date || userDetails.visits[0]?.date);
        // console.log("26. Update User Details Last Visit")
    }

    //FINALLY, WE PUSH ALL THE CHANGES TO THE USER'S LOCAL STORAGE
    // console.log("27. Push All Changes to Local Storage")
    // console.log("28. User Details is",userDetails)
    // console.log("29.1 TYPE OF USERDETAILS", typeof userDetails)
    const encrypted = await encryptString(JSON.stringify(userDetails))
    // console.log("29. Encrypted value is",encrypted)
    window.localStorage.setItem('journey', encrypted);
    // console.log("30. Add to LocalStorage", encrypted)
    return;
}
const sendToSlack = async (customerActivity, pathname) => {

    const message = {
        "path": pathname,
        "event": "pageview",
        "user": customerActivity
    }
    try {
        const response = await fetch('/api/slack/customer-activity', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Set the content type to JSON
            },
            body: JSON.stringify(message), // Convert form data to JSON
        });

        // console.log("Slack response:", response.status);

        // If needed, you can handle the response or check for errors here.

        return response;
    } catch (error) {
        console.error('Error sending data to Slack:', error);
        throw error; // Re-throw the error to handle it further if needed.
    }
}



// START READING FROM HERE 
export default function UserDetails({ params }) {
    
    // console.log("1. Starting point")
    // Grabbing the visitorDetailsContext to be able to read and push visitor info into Context().
    const { visitorDetails, addVisitorDetails, visitorDetailsLoaded, setVisitorDetailsLoaded } = visitorDetailsContext()
    
    //URL PATHNAME
    const pathname = usePathname()
    
    
    //URL PARAMETERS
    const searchParams = useSearchParams()
    const Ialocuuid = searchParams.get('qr_uuid')
    const source = searchParams.get('utm_source')
    const medium = searchParams.get('utm_medium')
    const campaign = searchParams.get('utm_campaign')
    const adset = searchParams.get('utm_adset')
    const ad = searchParams.get('utm_ad')
    const campaign_id = searchParams.get('utm_campaign_id')
    const adset_id = searchParams.get('utm_adset_id')
    const ad_id = searchParams.get('utm_ad_id')
    const fbclid = searchParams.get('fbclid')
    const gclid = searchParams.get('gclid')
    const ttclid = searchParams.get('ttclid')
    const urlParams = {
        source,
        medium,
        campaign,
        adset,
        ad,
        campaign_id,
        adset_id,
        ad_id,
        fbclid,
        gclid,
        ttclid,
        "cookies": params
    };

    /* //WRITE
     useEffect(() => {
         //Refresh when visitorDetails from context is updated.
         //console.log(visitorDetails)
     }, [visitorDetails])*/

    //READ
    // the whole logic of this useEffect is to grab or update user details on every pageview
    useEffect(() => {
        //    console.log("2. Inside Use Effect")
        const UserDetails = async () => {
            // console.log("3. User Details function started")        // Find User Details in localStorage
            let unDecodedUserDetails = await getUserDetails();
            // console.log("10. UseEffect unDecodedUserDetails", unDecodedUserDetails)
            // If there is nothing in storage (aka. new user)
            if (unDecodedUserDetails == null) {
                // console.log("11. If UndecodeduserDetails is null")
                // Call the first time user function

                // console.log("12. Start First Time User")
                firstTimeUser(urlParams, pathname);
                // console.log("13. Continue after FirstTimeUser")
                let Decoded = await getUserDetails();// Update userDetails after firstTimeUser
                // console.log("15. Decrypt again", unDecodedUserDetails)
                addVisitorDetails(Decoded);
                // console.log("16. Push to Context user Details", unDecodedUserDetails)
                setVisitorDetailsLoaded(true)
                // console.log("17. Set Visitor Details Loaded", setVisitorDetailsLoaded)

            }
            // If there is something in storage (aka. returning user)
            if (unDecodedUserDetails !== null) {
                // console.log("18. If unDecodedUserDetails is not null")
                // Add details to his visit & session count.
                appendDetails(unDecodedUserDetails, urlParams, pathname);
            }
            // console.log("31. Send to Slack", unDecodedUserDetails, pathname)
            // After every decision, push the user info to visitor context to be available everywhere.
            // fingerprintjs.then(fp => fp.get())
            //     .then(result => {
            //         const visitorId = result.visitorId
            //         thumbmark.then(fp => {
            //             unDecodedUserDetails.UDSID = visitorId + "-" + fp;
            //         })
            //     })

            unDecodedUserDetails.pathname = pathname

            insertBigQueryPageView(unDecodedUserDetails,Ialocuuid)
            //sendToSlack(unDecodedUserDetails, pathname);
            addVisitorDetails(unDecodedUserDetails);
            // console.log("32. Add Visitor Details", unDecodedUserDetails)
            setVisitorDetailsLoaded(true)
            // console.log("33. setVisitorDetailsLoaded", setVisitorDetailsLoaded)

            return
        }
        UserDetails()


        // below, whenever the pathname changes (aka. user changes pages) the whole decision tree is loaded.
    }, [pathname]);



    return (
        <>
        </>
    )
}

