'use client'
import { Button } from "@/components/ui/button"
// Use 'import' instead of 'require'
import { useState } from "react";

function EventForm() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phone: '',
  });
  const [formSent, setFormSent] = useState(false)

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  async function onSubmit(event) {
    setFormSent(true)
    event.preventDefault();
    const response = await fetch('/api/resend/evenimente', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json', // Set the content type to JSON
      },
      body: JSON.stringify(formData), // Convert form data to JSON
    });
  }

  return (
    <>
      <div className='m-5 '>
        <div className='w-full md:p-20 p-5 '>
          <div className=' pb-5 mr-20'>
            <h2 className='text-4xl font-bold '>Cere o Ofertă</h2>
            <p className=" md:mr-[30%] mt-2">Trimite-ne un email și în scurt timp vei avea cea mai tare petrecere. </p>
          </div>
          <a href="mailto:florin.vasilescu@piatto77.ro">
          <Button className="bg-orange-400 px-6 py-4">
             Cere Oferta
          </Button>
          </a>
        </div>
      </div>
    </>
  );
}

export default EventForm;
