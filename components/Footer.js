'use client'
import Link from "next/link";
import { usePathname } from 'next/navigation'
import SocialIcons from "./SocialIcons";
import MobileFooter from "./MobileFooter";
import Image from "next/image";
import { AspectRatio } from "@radix-ui/react-aspect-ratio";



function Footer() {

    const pathname = usePathname()

    function CheckMenuPath() {
        if (pathname.includes('meniu')) {
            return true;
        } else {
            return false
        }
    }
    const avoidMobileFooter = CheckMenuPath();

    return (
        <>
            <div className="flex flex-col box-border">

                <div className="flex flex-col px-1 md:px-4 lg:px-8">
                    <div className="container mx-auto border-2 rounded-xl bg-gray-50 flex flex-col justify-between mb-[100px] md:my-0 py-[60px] min-h-[500px]">

                        <div className="flex flex-col text-center justify-evenly ">
                            <div className="flex flex-row w-full">
                                <div className="w-1/2 flex flex-col">
                                    <h4 className="text-xl mt-4 md:mt-0">Locatie</h4>
                                    <p>Str. Polonă 77</p>
                                    <p>Sector 1, București</p>
                                </div>
                                <div className="flex flex-col w-1/2">
                                    <h4 className="text-xl mt-4 md:mt-0">Program</h4>
                                    <p>Luni - Vineri</p>
                                    <p>10:00 - 22:00</p>
                                    <p>Sâmbătă</p>
                                    <p>11:00 - 22:00</p>
                                    <p>Duminică</p>
                                    <p>11:00 - 21:00</p>

                                </div>
                            </div>
                            <div className="flex flex-row w-full justify-center">
                                <div className=" flex flex-col w-1/2">
                                    <h4 className="text-xl mt-4 md:mt-0">Navigație</h4>
                                    <Link href="/">Acasă</Link>
                                    <Link href="/meniu">Meniu</Link>
                                    <Link href="/urban-kitchen">Urban Kitchen</Link>
                                    <Link href="/contact">Contact</Link>
                                </div>
                                <div className=" flex flex-col w-1/2 justify-center">
                                    <Link href="/politica-de-confidentialitate">Politică Confidențialitate</Link>
                                    <Link href="/politica-cookies">Politica de Cookies</Link>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-row gap-4 align-middle justify-center lg:justify-around py-8">
                            <div className="w-1/2 md:w-2/5 lg:w-40 min-h-[50px] ">
                                <Link href="https://anpc.ro/ce-este-sal/">
                                    <AspectRatio ratio={9 / 2} className="">
                                        <Image src="/anpc/sal.png" fill={true} alt="ANPC SAL logo"></Image>
                                    </AspectRatio>
                                </Link>
                            </div>
                            <div className="w-1/2 md:w-2/5 lg:w-40 min-h-[50px]">
                                <Link href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=RO">
                                    <AspectRatio ratio={9 / 2} className="">
                                        <Image src="/anpc/sol.png" fill={true} alt="ANPC SOL logo"></Image>
                                    </AspectRatio>
                                </Link>
                            </div>
                        </div>
                        <div className="flex flex-col md:flex-row text-center justify-evenly mt-4 md:mt-0  ">
                            <SocialIcons></SocialIcons>
                            <div className="pt-10">
                                <p className="text-sm">Copyright © PIATTO 77 | S.C. Bratanii Food S.R.L.</p>
                                <div className="text-[8px]"><Link href='https://alexgolub.com' target="_blank"> Made by Alex Golub</Link></div>
                            </div>
                        </div>
                    </div>
                    {avoidMobileFooter == false ? (<MobileFooter></MobileFooter>) : (<></>)}

                </div>
            </div>
        </>
    )
}

export default Footer;