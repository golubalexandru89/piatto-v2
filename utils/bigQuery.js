

export const insertBigQueryProductView = async (details, timeDifference, visitorDetails) => {
    //console.log("TIMEDIF",timeDifference.toFixed(0))
    const oldObject = details
    oldObject.view_time = timeDifference.toFixed(0)
    const timestamp = new Date()

    const object = {
        "Unique Pseudo Visitor ID UPVID": visitorDetails.puuid,
        "Timestamp": timestamp,
        "Page URL": visitorDetails.pathname,
        "Category": details.category,
        "Category Order": details.categoryOrder,
        "Description English": details.descriptionEn,
        "Description Romanian": details.descriptionRo,
        "ID": details.id,
        "Image URL": details.imageURL,
        "Is Available": details.isAvailable,
        "Is POS Available": details.isPosAvailable,
        "Name English": details.nameEn,
        "Name Romanian": details.nameRo,
        "Price": details.price,
        "Product": details.product,
        "Product Featured": details.productFeatured,
        "Subcategory": details.subCategory,
        "View Time": details.view_time,
        "Visual Order Index": details.visualOrderIndex,
        "Weight": details.weight,
        "Weight Unit": details.weightUnit,
    }


    fetch('/api/bigQuery/productView', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json', // Set the content type to JSON
        },
        body: JSON.stringify(object), // Convert form data to JSON
    });
}

export const insertBigQueryPageView = async (details,uuid) => {

    const timestamp = new Date()

    const object = {
        "Unique Pseudo Visitor ID UPVID": details.puuid,
        "Timestamp": timestamp,
        "Page URL": details.pathname,
        "Ialoc uuid":uuid
    }
    //console.log("OBJECT IS",object)


    try {
        // Insert data into a table
        const response = await fetch('/api/bigQuery/pageView', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Set the content type to JSON
            },
            body: JSON.stringify(object), // Convert form data to JSON
        });
        // Insert data into a table
        //.createDataset('test',{location:"US"})
        //.dataset('Piatto_User_Behaviour').table('raw_data').insert({data:0,name:0});
        //console.log(answer);
        return response
    } catch (error) {
        // console.error('Error inserting rows:', error.response.insertErrors[0].errors);
    }

}

export const insertBigQueryPromotionView = async (details, timeDifference, visitorDetails) => {
    const oldObject = details
    oldObject.view_time = timeDifference.toFixed(0)

    const timestamp = new Date()

    const object = {
        "Unique Pseudo Visitor ID UPVID": visitorDetails.puuid,
        "Timestamp": timestamp,
        "Page URL": visitorDetails.pathname,
        "View Time": details.view_time,
        "Promotion": details.promotion,
        "Promotion URL": details.url,
    }


    try {
        // Insert data into a table
        const response = await fetch('/api/bigQuery/promotionView', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', // Set the content type to JSON
            },
            body: JSON.stringify(object), // Convert form data to JSON
        });
        return response
    } catch (error) {
        //console.error('Error inserting rows:', error.response.insertErrors[0].errors);
    }

}
