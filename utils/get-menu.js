import {cache} from 'react';
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const revalidate = 15;

export const getMenu = cache(async () => {
    const menu = await prisma.meniu.findMany({
        skip: 0,
        take: 5000,
      })
   // console.log(menu)
    return menu
})