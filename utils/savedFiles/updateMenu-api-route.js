import axios from "axios";
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

export async function GET(req, res) {
    try {
        const prisma = new PrismaClient();
        const url = 'https://cloud.gravitylabs.ro/gravity/v1/menu/?t=1698950477410';
        const headers = {
            Accept: 'application/json, text/plain, */*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ro-RO,ro;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6,de;q=0.5',
            Connection: 'keep-alive',
            Consumer: 'slice',
            Host: 'cloud.gravitylabs.ro',
            Origin: 'https://gravitylabs.ro',
            Planetidentifier: 'piatto77220722',
            Referer: 'https://gravitylabs.ro/',
            'Sec-Ch-Ua': '"Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"',
            'Sec-Ch-Ua-Mobile': '?0',
            'Sec-Ch-Ua-Platform': '"macOS"',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
        }
        async function getProducts() {
            try {
                const response = await axios.get(url, { headers });
                console.log("API Update Menu function invoked")
                console.log('Response Status Code:', response.status);
                const data = response.data;

                const result = {};

                data.Menu.Items.forEach((productItem) => {
                    const [mainCategory, subCategory, subSubCategory] = productItem.PosCategory.split("\\");

                    if (!result[mainCategory]) {
                        result[mainCategory] = {};
                    }
                    if (!result[mainCategory][subCategory]) {
                        if (subSubCategory == undefined) {
                            result[mainCategory][subCategory] = [];
                        } else {
                            result[mainCategory][subCategory] = {};
                        }
                    }
                    if (subSubCategory == undefined) {
                        result[mainCategory][subCategory].push(productItem);
                        //console.log(result[mainCategory][subCategory])
                    } else {
                        if (!result[mainCategory][subCategory][subSubCategory]) {
                            result[mainCategory][subCategory] = [];
                        }
                        result[mainCategory][subCategory].push(productItem);
                    }
                });

                //console.log(result)
                let productList = result

                // Now that each product has been split, it's time to loop through the object and send to DB.

                //// --- Below: It takes each product from the object --- 
                for (const mainCategory in productList) {
                    //console.log(`Main Category: ${mainCategory}`);

                    // Loop through subcategories (category1, category2, ...)
                    for (const subCategory in productList[mainCategory]) {
                        //console.log(`  Subcategory: ${subCategory}`);

                        // Loop through products in each subcategory
                        for (const product in productList[mainCategory][subCategory]) {
                            //console.log(`    Product: ${product}`);
                            const details = productList[mainCategory][subCategory][product];

                            if (subCategory.includes("WINE")) {
                                continue;
                            }
                            // Send each item to DB
                            //console.log('      Details:', details);
                            const checkItem = await prisma.meniu.update({
                                where:{
                                    id:Number(details.PosItemIdentifier)
                                },
                                data:{
                                    updatedAt:new Date().toISOString(),
                                    category:mainCategory,
                                    subCategory:subCategory,
                                    product:details.PosName,
                                    isPosAvailable:details.IsPosAvailable,
                                    itemIdentifier:details.ItemIdentifier,
                                    nameRo:details.Names?.ro||"",
                                    nameEn:details.Names?.en||details.Names?.ro||"",
                                    descriptionRo:details.Descriptions?.ro||"",
                                    descriptionEn:details.Descriptions?.en||details.Descriptions?.ro||"",
                                    isAvailable:details.IsAvailable,
                                    price:details.Price,
                                    weight:Number(details.WeightMeasure?.Weight)||0,
                                    weightUnit:details.WeightMeasure?.UnitMeasure||"gr",
                                    imageURL:details.ImagePath
                                }
                            })

                        }
                    }
                }

                return { "Menu Updated": true };

            } catch (error) {
                console.error('Error:', error);
                return { error: "An error occurred" };
            }
        }

        const response = await getProducts();
        return NextResponse.json(response);

    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred at the top level" });
    }
}