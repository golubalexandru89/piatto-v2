// utils/LanguageContext.js
'use client'
import React, { createContext, useContext, useState } from 'react';

const VisitorDetails = createContext();

export const visitorDetailsContext = () => {
  return useContext(VisitorDetails);
};

export const VisitorDetailProvider = ({ children }) => {
  const [visitorDetails, setVisitorDetails] = useState({});

  const [visitorDetailsLoaded, setVisitorDetailsLoaded] = useState(false)

  const addVisitorDetails = (input) => {
    setVisitorDetails(input);
  };

  const value = {
    visitorDetails, // Fix the variable name here
    visitorDetailsLoaded,
    setVisitorDetailsLoaded,
    addVisitorDetails,
  };

  return (
    <VisitorDetails.Provider value={value}>
      {children}
    </VisitorDetails.Provider>
  );
};
