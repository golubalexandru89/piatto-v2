// utils/LanguageContext.js
"use client"
import React, { createContext, useContext, useState } from 'react';

const LanguageContext = createContext();

export const useLanguageContext = () => {
  return useContext(LanguageContext);
};

export const LanguageProvider = ({ children }) => {
  const [language, setLanguage] = useState("ro");

  const toggleLanguage = () => {
    setLanguage((prevLanguage) => (prevLanguage === "ro" ? "en" : "ro"));
  };

  const value = {
    language,
    toggleLanguage,
  };

  return (
    <LanguageContext.Provider value={value}>
      {children}
    </LanguageContext.Provider>
  );
};
