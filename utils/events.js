export const gtagEvent = (name, options = {}, time) => {
    options.view_time = time;
    delete options.createdAt
    delete options.updatedAt
    /*let oldPrice = options.price
    let newPrice = oldPrice / 100
    options.price = newPrice*/

    window.gtag("event", name, options);
};

export const promotionViewEvent = (name, options = {}, time) => {
    if (time < 800) {
        return
    }

    delete options.type
    delete options.duration
    options.view_time = time
    //console.log(options)


    window.gtag("event", name, options)
}

export const setIalocUuid = (uuid) => {
    window.gtag("set", "user_properties", {
        'ialoc_uuid': uuid
    })
}