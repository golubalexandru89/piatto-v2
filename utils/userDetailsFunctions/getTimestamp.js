export default function GetTimeStamp(){
    const dateNow = new Date();
  
    // Format date as dd-mm-yyyy
    const day = String(dateNow.getDate()).padStart(2, '0');
    const month = String(dateNow.getMonth() + 1).padStart(2, '0');
    const year = dateNow.getFullYear();
    const formattedDate = `${day}-${month}-${year}`;
  
    // Format time as hh-mm
    const hours = String(dateNow.getHours()).padStart(2, '0');
    const minutes = String(dateNow.getMinutes()).padStart(2, '0');
    const formattedTime = `${hours}:${minutes}`;
  
    // Return an object with the formatted date and time
    return {
      date: formattedDate,
      hour: formattedTime,
    };
}