// utils/userDetailsFunctions/getUserDetails.js

import CreateUserId from "./createUserId";
import GetTimeStamp from "./getTimestamp";



export const getUserDetails = () => {


  //console.log("get user details started");
  try {
    // Check if we are in a browser environment
    if (typeof window !== 'undefined' && window.localStorage) {
      //console.log("localStorage is available");

      const userDetailsString = localStorage.getItem('journey');


      if (userDetailsString === null) {
        //initialize the new object
        let newData = {}

        //generate a new user id and add to object
        const UUID = CreateUserId();
        newData.puuid = UUID;

        const { date, time } = GetTimeStamp();
        newData.last_visit_time = date;
        newData.days_since_last_visit = 0;

       
        newData.first = {}
        newData.first.source = "Facebook"

        //add data to localStorage
        window.localStorage.setItem('journey', JSON.stringify(newData))


        return
      }

      const userDetails = JSON.parse(userDetailsString);

      return userDetails;
    } else {
      console.warn('localStorage is not available in this environment.');
      return null;
    }
  } catch (error) {
    console.error('Error while getting user details from localStorage:', error);
    return null;
  }
};

export const setUserDetails = () => {

  // window.localStorage.setItem('name',JSON.stringify({'dude':'ALEX','height':{'maybe':'not'}}))
}