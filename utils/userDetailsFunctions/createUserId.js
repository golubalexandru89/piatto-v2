import crypto from 'crypto'

export default function CreateUserId() {

    const buf = crypto.randomBytes(6);
    // Set version to 4 (random UUID)
    buf[6] = (buf[6] & 0x0f) | 0x40;
    // Set variant to RFC4122
    buf[8] = (buf[8] & 0x3f) | 0x80;



    return buf.toString('hex').match(/.{1,2}/g).join('-');
}
