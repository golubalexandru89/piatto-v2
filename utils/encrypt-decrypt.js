import { encryptString, decryptString } from "./crypto"

export async function encrypting() {
    const test = await encryptString("asd");
    test
        .then(
            res => {
                return res
            }
        ).catch(
            error => console.error(error)
        )
}

export async function decrypt() {
    const test = await decryptString("asd");
    test.then(res => { return res }).catch(error => console.error(error))

}