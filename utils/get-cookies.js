import { cookies } from 'next/headers'

export default function GetCookies() {
    const cookieList = ['_ga', '_ttp', '_fbp', '_ga_1W5T8ZP7SJ','_gcl_au']
    const cookieStore = cookies()
    let cookiesFound = []
    for (let i = 0; i < cookieList.length; i++) {
        const value = cookieStore.get(cookieList[i]);
        if (value !== undefined){
            cookiesFound.push(value)
        }
    }
    return cookiesFound;
}