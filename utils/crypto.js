'use server'
require('dotenv').config();
const crypto = require('crypto');
const Key = process.env.REACT_APP_CRYPTO

function adjustKeySize(secretKey) {
  if (secretKey.length < 32) {
    // If the key is shorter than 32 bytes, pad it with zeros
    secretKey = Buffer.concat([Buffer.from(secretKey), Buffer.alloc(32 - secretKey.length)]);
  } else if (secretKey.length > 32) {
    // If the key is longer than 32 bytes, truncate it
    secretKey = secretKey.slice(0, 32);
  }
  return secretKey;
}

export async function encryptString(originalString) {
  let secretKey = adjustKeySize(Buffer.from(Key));
  const cipher = crypto.createCipheriv('aes-256-ecb', secretKey, Buffer.alloc(0));

  let paddedData = Buffer.concat([Buffer.from(originalString, 'utf-8')]);
  const blockSize = 16;
  const padding = blockSize - (paddedData.length % blockSize);
  paddedData = Buffer.concat([paddedData, Buffer.alloc(padding, padding)]);

  let encryptedData = cipher.update(paddedData);
  encryptedData = Buffer.concat([encryptedData, cipher.final()]);

  return encryptedData.toString('base64');
}

export async function decryptString(encryptedString) {
  let secretKey = adjustKeySize(Buffer.from(Key));
  const decipher = crypto.createDecipheriv('aes-256-ecb', secretKey, Buffer.alloc(0));

  const encryptedData = Buffer.from(encryptedString, 'base64');
  let decryptedData = decipher.update(encryptedData);
  decryptedData = Buffer.concat([decryptedData, decipher.final()]);

  const padding = decryptedData[decryptedData.length - 1];
  decryptedData = decryptedData.slice(0, decryptedData.length - padding);

  return decryptedData.toString('utf-8');
}
/*
const yourSecretKey = 'abcd'; // Note: Use a string for the secret key
const originalString = 'bla bla bla';

const encryptedString = encryptString(originalString, yourSecretKey);
console.log('Encrypted:', encryptedString);

const decryptedString = decryptString(encryptedString, yourSecretKey);
if (decryptedString !== null) {
  console.log('Decrypted:', decryptedString);
} else {
  console.log('Decryption failed.');
}*/
