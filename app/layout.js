import { Inter } from 'next/font/google'
import './globals.css'
import Header from '../components/Header'
import Footer from '../components/Footer'
import { LanguageProvider } from '../utils/contextProviders/LanguageContext'
import { VisitorDetailProvider } from '../utils/contextProviders/VisitorDetailsContext'
import UserDetails from '../components/UserDetails'
import GetCookies from "../utils/get-cookies"
import FacebookPixel from '../components/MarketingScripts/FacebookPixel'
import GoogleAdsPixel from '../components/MarketingScripts/GoogleAds'
import TikTokPixel from '../components/MarketingScripts/TikTokPixel'
import { Toaster } from "react-hot-toast";
import AnnouncementToaster from '../components/AnnouncementToaster'



const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  metadataBase: new URL('https://piatto77.ro')
}

export default function RootLayout({ children }) {
  const cookie = GetCookies()

  return (
    <VisitorDetailProvider>
    <LanguageProvider>
      <html lang="en">
        <body className={inter.className}>
          <FacebookPixel />
          <GoogleAdsPixel/>
          <TikTokPixel/>
          <Header />
          
          <UserDetails params={cookie}></UserDetails>
          <Toaster position="bottom-center" />
          {/*<AnnouncementToaster></AnnouncementToaster> */}
          {children}
          <Footer />
        </body>
      </html >
    </LanguageProvider>
    </VisitorDetailProvider>
  )
}
