import axios from "axios";
import { NextResponse } from "next/server";
import { parseString } from "xml2js";

export async function POST(req, res) {
    // Assuming the incoming data is in the request body
    const xmlData = await req.text();

    // Parse XML data to JavaScript object
    let parsedData;
    try {
        parsedData = await parseXml(xmlData);
        console.log(parsedData);
    } catch (error) {
        console.error("Error parsing XML:", error);
        return NextResponse.text("Error parsing XML", { status: 400 });
    }

    // Process the parsed data as needed

    return NextResponse.json("Webhook received", { status: 200 });
}

// Function to parse XML using xml2js library
function parseXml(xmlData) {
    return new Promise((resolve, reject) => {
        parseString(xmlData, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}
