import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(req, res) {
    const body = await req.json();

    try {
        const slackWebhookUrl = 'https://hooks.slack.com/services/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';

        // Assuming req.body is an object with properties like 'name', 'email', 'phone'
        const postData = {
            blocks: [{
                type:"section",
                text:{
                    type:"mrkdwn",
                    text:`------------ \n New Event Submission`
                }
            },{
                type: "section",
                text: {
                    type: "mrkdwn",
                    text: `Name: ${body.name}`
                },
            }, {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text:`Email: ${body.email}`}
            }, {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text:`Phone: ${body.phone}`}
            }
            ]
        };

        const response = await axios.post(slackWebhookUrl, postData, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        console.log("SENT SUCCESSFULLY");
        return NextResponse.json({ nice: "job", res: res });
    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred" });
    }
}