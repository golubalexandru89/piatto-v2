import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(req, res) {
    const body = await req.json();

    try {
        const slackWebhookUrl = 'https://hooks.slack.com/services/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';

        // Assuming req.body is an object with properties like 'name', 'email', 'phone'
        const postData = {
            blocks: [{
                type:"section",
                text:{
                    type:"mrkdwn",
                    text:`------------ \n New Newsletter Signup`
                }
            }, {
                type: "section",
                text: {
                    type: "mrkdwn",
                    text:`Email: ${body.email}`}
            }
            ]
        };

        const response = await axios.post(slackWebhookUrl, postData, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        console.log("SENT SUCCESSFULLY");
        return NextResponse.json({ nice: "job", res: res });
    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred" });
    }
}