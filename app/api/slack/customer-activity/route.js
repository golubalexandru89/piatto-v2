import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(req, res) {
    const body =  await req.json();
    //console.log("Body is ",body)
    const user =  body.user.puuid
    const page =  body.path
    const lastSessionTime = body.user.last_session_time 
    const daysSinceLastSession = body.user.days_since_last_session
    const totalVisits = body.user.total_number_of_visits || 1
    const daysSinceLastVisit = body.user.days_since_last_visit || `NU A FOST`
    const lastVisitTime = body.user.last_visit_time || `NU A FOST`
    const message = `Userul cu ID-ul ${user} a vizitat pagina ${page}.Asta este vizita numărul ${totalVisits}. Ultima vizită a fost în ${lastVisitTime} în data ${daysSinceLastVisit}. Tot el/ea a vizitat website-ul acum ${daysSinceLastSession} zile, adică ${lastSessionTime}`
   // console.log('Message:', message)
    


    try {
        const slackWebhookUrl = 'https://hooks.slack.com/services/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';

        // Check if the request body is an empty object
        if (Object.keys(body).length === 0 && body.constructor === Object) {
            return NextResponse.json({ message: "empty object" });
        }

        // Assuming req.body is an object with properties like 'name', 'email', 'phone'
        const postData = {
            text: JSON.stringify(body)
        };

        const response = await axios.post(slackWebhookUrl, postData, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        return NextResponse.json({ nice: "job", res: res });
    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred" });
    }
}
