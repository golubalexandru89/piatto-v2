import axios from "axios";
import { NextResponse } from "next/server";

export async function POST(req, res) {
    const body =  await req.json();
    


    try {
        const slackWebhookUrl = 'https://hooks.slack.com/services/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';

        // Check if the request body is an empty object
        if (Object.keys(body).length === 0 && body.constructor === Object) {
            return NextResponse.json({ message: "empty object" });
        }

        // Assuming req.body is an object with properties like 'name', 'email', 'phone'
        const postData = {
            text: JSON.stringify(body)
        };

        const response = await axios.post(slackWebhookUrl, postData, {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        return NextResponse.json({ nice: "job", res: res });
    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred" });
    }
}
