import { BigQuery } from '@google-cloud/bigquery';
import { NextResponse } from 'next/server';


export async function POST(req, res) {
    const body = await req.json()

    const creds = {
        "type": "service_account",
        "project_id": "piatto-77",
        "private_key_id": "d82b5e92d41085d71890179068ece757b4b03ec4",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQClmchyUAgN91Bh\n7GCe1U99/2hxhgfXpWoAALFekoysnULOGyYZgY9IQGSmdQq5nj+XTe3tU3TjC+Nc\nKzQ7KkTAy5J9uGazLufSrKWG2itu2g8v2MnZHEB6QenfTivS/CqmWZqV8ID6nGk0\nomKjdOPAqmOXiLIzMhQzaaqbQKMQusnUm75nG5ZKrzkARY8wu9QectHR1fCLIWtl\nqrot50Fkc/SG82eSCO5Iz6Zei+BuQBkAIBuHE7O7S5Aww5mQ4b6/IplZCAGzpOK0\nKNYrmpWAU8DU3IpL9sWqDJHzK1aCYJLCc/I7X0I7lLemb1L7yKsGNzH5Na0o6Ofq\n5lMrmdXBAgMBAAECggEAQ0+XVLOoGnkosKzidx0wQwAiZWZ74JEJXXfqoIzD5lPD\nuD/7xAlQAqe94ZUYZ2tl4KvZ5onyLGmEIE4lXFkpeg4k/LoL8JBDxXPMklwypEu5\nOZMONJQRtz/8fnppQMij01r/hSutWXBjKPRKULNt8b2wXoM/H6Ue5ejw4ksrHNyP\nerD9Cwg7tBzxoUTsUiBaXuDLmKq4yzq5ku+W+dLFImbBL0syNopxyhLt9mm6U8yM\nEw+sAz1l6ngTIRRKj18pOCtcViV6K+U+juwxyjGWVeX+qQigHTlfZB3wZZfBdujd\nc3/RIq1+UGFoW/aXFFJvYDfckeYifsCtQu26JmWamwKBgQDjt3ls47L90ELwYGYy\nxS2inrribW9tgnGVG1qRsppjU0d4zinub4dRNC7lg6qup2mCsN6PqZ3SONNn3ods\nFpwjJA/4rUwmIZUT9N3tirFmfOShcflyTj+/+nki559CMZDikNjDGekURDaj6BlO\nARLCok7STXoads9g4u4Mbxm9BwKBgQC6K0Go38sI8UZtTe0d+RAwnM/HNSxQKvnH\nir4oIbeFdH2tuk+1LAOXry5bypawfvNhYDcw0cGUHFtXLI3vRjxRmYAwJnszzXVF\nozqBxukcfW/C2FW1L42lBHbJKMA/XN0yWGjOcC/PLr0yMwXgDGItbF3+kAPbVwSS\niJFzpe7s9wKBgB2UWqiDV1/S6q8QxpW3uTrThZEeCAhglmJ5ACXG3nkiezk9BoNb\nZ2dr8N+NX+F6C6GXWgGtVbvV99gZNtwaf8OszopN8INPf27tMtrFVk/qz0QNawqn\nEW7dmVioX66Yg6p6iD2novgn+DUIVCkmyIoRzU1SFRdU8ZEzSRrCX8SHAoGABIz+\nI+Skj+ll8VIQU8U80hNjONlh5evHA0uu7kg0tjxsvgxp2mHjDJgpp/DB0jqoNF8n\nujfGZZpz2SOrrIeo/c2kxoiKRm/lR/h/e6CjHiEq1DZ5kd5BtiDvzmcCnAnnFk32\n8eToJkZcNZftstZaqAWYHprinT/5SqOv+oW5220CgYBW2zbj+ubpJjmI9h76QkB1\nm9g834NTVgBKgfIqoR1ObVX1SJRZ+Nx3M59Bx7GeISbzgL+Lf+kICJBIQp/AnGiD\nKkVeFWvDsZnC9LwIlBRzIygJ+Ll0qoGElap84TnriqKv5ImoMti3NZGW0xcAc8H4\ne25F4K3u2R8CZhRgGDzjsA==\n-----END PRIVATE KEY-----\n",
        "client_email": "test-218@piatto-77.iam.gserviceaccount.com",
        "client_id": "104365686420599454405",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/test-218%40piatto-77.iam.gserviceaccount.com",
        "universe_domain": "googleapis.com"
    }





    try {
        const bigquery = new BigQuery({
            credentials: creds,
            projectId: 'piatto-77',
        });

        bigquery.dataset('Piatto_User_Behaviour').table('raw_data').insert(body);


        return NextResponse.json({ nice: "Promotion View", res: res });
    } catch (error) {

        return NextResponse.json({ error: "An error occurred" });
    }
}