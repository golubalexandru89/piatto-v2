import axios from "axios";
import { NextResponse } from "next/server";
import { Resend } from 'resend';
import evenimente from "../../../../emails/evenimente";


export async function POST(req, res) {
    const body =  await req.json();
    console.log(process.env.REESEND_API_KEY)
    console.log(body)

    const resend = new Resend(process.env.REESEND_API_KEY);

    try {
        resend.emails.send({
            from: 'marketing@piatto77.ro',
            to: 'it@piatto77.ro',
            subject: 'Cerere de Eveniment',
            react:evenimente(body)
          });

        return NextResponse.json({ nice: "job", res: res });
    } catch (error) {
        console.error(error);
        return NextResponse.json({ error: "An error occurred" });
    }
}
