/*

TO DO:
- change the code so that instead of inserting new item, to upload the actual one. 
- see how to daily chron the price update. 
- see how to pull data from db
- see how to display the menu on page. 



This internal tool is grabbing all the items from the Rkeeper POS system 
and pushes them to Planetscale DB with prisma. 
The data comes from the api on https://gravitylabs.ro/menuonly/#/?theme=images&planetIdentifier=piatto77220722
*/

// Loading Requirements
// Axios
const axios = require('axios');

//Prisma
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

// Rkeeper POS 
const url = 'https://cloud.gravitylabs.ro/gravity/v1/menu/?t=1698950477410';
const headers = {
    Accept: 'application/json, text/plain, */*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ro-RO,ro;q=0.9,en-US;q=0.8,en;q=0.7,it;q=0.6,de;q=0.5',
    Connection: 'keep-alive',
    Consumer: 'slice',
    Host: 'cloud.gravitylabs.ro',
    Origin: 'https://gravitylabs.ro',
    Planetidentifier: 'piatto77220722',
    Referer: 'https://gravitylabs.ro/',
    'Sec-Ch-Ua': '"Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"',
    'Sec-Ch-Ua-Mobile': '?0',
    'Sec-Ch-Ua-Platform': '"macOS"',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-site',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
};

/*
High-level view!
The below function calls the api url and returns the products in an object. 
It takes that object and splits the each item by the POSCategory value. 
The reason is that POSCategory is made of "BAR//CATEGORY//PRODUCTdetails"
After the split, it pushes the data into an object. 
That object is then parsed and each item is pushed to DB.
*/

async function getProducts() {
    try {
        const response = await axios.get(url, { headers });
        //console.log('Response Status Code:', response.status);
        const data = response.data;

        //console.log(data.Menu.Items[20])
        const result = {};

        //loop through the object and split the item into it's specific category and subcategory. 
        data.Menu.Items.forEach((productItem) => {
            const [mainCategory, subCategory, subSubCategory] = productItem.PosCategory.split("\\");

            if (!result[mainCategory]) {
                result[mainCategory] = {};
            }
            if (!result[mainCategory][subCategory]) {
                if (subSubCategory == undefined) {
                    result[mainCategory][subCategory] = [];
                } else {
                    result[mainCategory][subCategory] = {};
                }
            }
            if (subSubCategory == undefined) {
                result[mainCategory][subCategory].push(productItem);
                //console.log(result[mainCategory][subCategory])
            } else {
                if (!result[mainCategory][subCategory][subSubCategory]) {
                    result[mainCategory][subCategory] = [];
                }
                result[mainCategory][subCategory].push(productItem);
            }
        });

        //console.log(result)
        productList = result

        // Now that each product has been split, it's time to loop through the object and send to DB.

        //// --- Below: It takes each product from the object --- 
        for (const mainCategory in productList) {
            //console.log(`Main Category: ${mainCategory}`);

            // Loop through subcategories (category1, category2, ...)
            for (const subCategory in productList[mainCategory]) {
                //console.log(`  Subcategory: ${subCategory}`);

                // Loop through products in each subcategory
                for (const product in productList[mainCategory][subCategory]) {
                    //console.log(`    Product: ${product}`);
                    const details = productList[mainCategory][subCategory][product];

                    // Send each item to DB
                    //console.log('      Details:', details);
                    try {
                        const checkItem = await prisma.meniu.update({
                            where: {
                                id: Number(details.PosItemIdentifier)
                            },
                            data: {
                                updatedAt: new Date().toISOString(),
                                category: mainCategory,
                                product: details.PosName,
                                isPosAvailable: details.IsPosAvailable,
                                itemIdentifier: details.ItemIdentifier,
                                nameRo: details.Names?.ro || "",
                                nameEn: details.Names?.en || details.Names?.ro || "",
                                descriptionRo: details.Descriptions?.ro || "",
                                descriptionEn: details.Descriptions?.en || details.Descriptions?.ro || "",
                                isAvailable: details.IsAvailable,
                                price: details.Price,
                                weight: Number(details.WeightMeasure?.Weight) || 0,
                                weightUnit: details.WeightMeasure?.UnitMeasure || "gr",
                                imageURL: details.ImagePath
                            }
                        })

                        //console.log(checkItem)
                    } catch (error) {
                        console.error('Error updating item:', error)
                        // Continue to the next item in case of an error

                        continue;
                    }
                }
            }
        }
    } catch (error) {
        console.log("ERROR",error)
        throw error;
    }
}
getProducts();
