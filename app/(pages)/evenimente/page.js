
import Image from 'next/image'
import EventForm from '../../../components/Events/EventForm'
import Link from 'next/link'

export const metadata = {
    title: "Evenimente Corporate și Petreceri Private | PIATTO 77 - Urban Kitchen ",
    description: 'Piatto 77 redefinește eleganța și modernitatea pentru evenimentele corporate, oferind un cadru amplu și sofisticat cu o capacitate ideală între 50-70 de persoane.',
    languages: "ro-Ro",
    openGraph: {
        title: "Evenimente Corporate și Petreceri Private | PIATTO 77 - Urban Kitchen ",
        description: 'Piatto 77 redefinește eleganța și modernitatea pentru evenimentele corporate, oferind un cadru amplu și sofisticat cu o capacitate ideală între 50-70 de persoane.',
        url: 'https://piatto77.ro/evenimente',
        siteName: 'PIATTO 77 - Urban Kitchen',
        images: [
            {
                url: '/hero-image.jpg',
                width: 800,
                height: 600,
            },
        ],
        locale: 'ro_RO',
        type: 'website',
    }
}
function EvenimenteCorporate() {


    return (
        <>
            <div className="container mx-auto h-1/2 flex flex-col justify-center text-center min-h-[300px]">
                <h2 className="text-4xl font-medium pb-2"> Descoperă Magia PIATTO 77</h2>
                <h1 className="text-5xl font-bold"> Evenimente Corporate și Petreceri Private</h1>
            </div>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/hero.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="image about Piatto 77 location" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 w-full md:py-20 py-5 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Locație Exclusivă și Elegantă</h3>
                        <p>Piatto 77 redefinește eleganța și modernitatea pentru evenimentele corporate, oferind un cadru amplu și sofisticat cu o capacitate ideală între 50-70 de persoane.</p>
                        <p>Situat central, acest loc excepțional se împletește cu istoria clădirii, creând o atmosferă distinctivă și memorabilă pentru fiecare eveniment.</p>
                        <p>În plus, beneficiind de o locație centrală, există locuri de parcare, oferind confort și accesibilitate tuturor participanților.</p>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Serviciu Personalizat</h3>
                        <p>Descoperiți serviciul nostru de evenimente corporate la Piatto 77, unde personalizarea este esențială.</p>
                        <p>Echipa noastră dedicată vă sprijină în ajustarea fiecărui detaliu, de la meniuri personalizate la personalul profesionist, asigurându-vă că orice tip de eveniment devine o experiență memorabilă.</p>

                    </div>
                    <div className="md:w-1/2 w-full  align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image priority={true} src="/evenimente/serviciu-personalizat.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="serviciu personalizat la urban kitchen piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative min-w-full min-h-[300px]'>
                            <Image src="/evenimente/bauturi-premium.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="bauturi la piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Băuturi Premium și Cocktail-uri Unice​</h3>
                        <p>Experimentați luxul băuturilor premium și al cocktail-urilor unice la Piatto 77. Cu o selecție vastă de băuturi premium, iar barmanii noștri experimentați vă vor încânta cu cocktail-uri pregătite cu măiestrie. </p>
                        <p>De la aperitive rafinate la băuturi clasice și inovatoare, vă invităm să descoperiți o experiență de degustare de excepție într-un cadru elegant și modern.</p>
                    </div>
                </div>
            </container>
            <container>
                <div className='container mx-auto bg-orange-400 min-h-[400px] flex flex-col justify-center align-center text-center text-white space-y-3 md:px-[15%] my-10 px-10'>
                    <h3 className='text-3xl md:text-4xl font-bold lg:px-24 md:px-12 px-10'>Completează Formularul pentru o Ofertă Personalizată</h3>
                    <p className='md:px-20 px-10'>Descoperă cum Piatto 77 poate aduce magia evenimentelor corporate în lumea ta. Completează formularul pentru a primi o ofertă personalizată care să îndeplinească toate nevoile tale.</p>
                    <div>
                        <Link href="#form">
                            <button className='bg-white text-black w-[200px] h-[50px] rounded-2xl align-center text-center'> Către Formular
                            </button>
                        </Link>
                    </div>
                </div>
            </container>
            <container>
                <div className='container mx-auto flex flex-col justify-center text-center align-center min-h-[300px] space-y-5'>
                    <h2 className='uppercase text-5xl font-bold'>Urban Kitchen</h2>
                    <h3 className='text-2xl font-semibold'>O Experiență Culinară Excepțională</h3>
                    <p className='md:px-[30%] px-10'>Bucurați-vă de meniul nostru rafinat, pregătit cu ingrediente proaspete și prezentat artistic pentru a satisface cele mai exigente gusturi.</p>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Originile Gătitului Urban Kitchen</h3>
                        <p>Bucătăria urbană a apărut în zonele urbane la începutul secolului al XX-lea. Bucătarii au folosit ingrediente de proveniență locală pentru a crea mâncăruri gustoase ca răspuns la industrializarea producției alimentare</p>

                    </div>
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center px-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px] '>
                            <Image src="/evenimente/originile-gatitului.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="origini gatit urban kitchen" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/evolutia-urban-kitchen.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Evoluția pe parcursul timpului</h3>
                        <p>Bucătăria urbană a evoluat pentru a încorpora tehnici și ingrediente din bucătăriile internaționale.</p>
                        <p>Ea pune accentul pe ingrediente proaspete și sănătoase și pe metode de gătit inovatoare, ceea ce o face din ce în ce mai populară pentru cei care caută experiențe culinare unice și pline de savoare.</p>

                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Influențe Importante</h3>
                        <p>Bucătarii de renume, printre care Jamie Oliver, Anthony Bourdain și Gordon Ramsay, au popularizat ingrediente proaspete, de proveniență locală și tehnici de gătit inovatoare din diferite bucătării internaționale.</p>
                        <p>Influența lor a ridicat bucătăria urbană la rang de fenomen global, inspirând bucătarii și iubitorii de mâncare deopotrivă să exploreze diverse arome.</p>
                    </div>
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/originile-gatitului.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="origini gatit urban kitchen" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className='container mx-auto flex flex-col justify-center text-center align-center min-h-[300px] space-y-5 pt-10'>
                    <h2 className='uppercase text-5xl font-bold'>ALĂTURI DE O ECHIPĂ FORMIDABILĂ</h2>
                    <p className='md:px-[30%] px-10'>Bucurați-vă de meniul nostru rafinat, pregătit cu ingrediente proaspete și prezentat artistic pentru a satisface cele mai exigente gusturi.</p>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-4xl font-semibold pb-5'>Executive Chef <br /> Marius Nicolae</h3>
                        <p>Cu o experiență de 19 ani în bucătărie, Marius a început ca bucătar și a ajuns chef la Palatul Snagov, unde a fost responsabil pentru summit-uri prezidențiale.</p>
                        <p>Cu 8 ani de experiență internațională la Dublin, Marius a adus o combinație distinctivă de arome românești și influențe internaționale în fiecare preparat, conturând o carieră dedicată în arta culinară.</p>
                    </div>
                    <div className="md:w-1/2 align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/marius-nicolae.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="marius co-founder piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/florin-vasilescu.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="florin co-founder piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-4xl font-semibold pb-5'>Experience Manager <br /> Florin Vasilescu</h3>
                        <p>Florin, fost Brand Ambassador la Heineken, a evoluat spre roluri de middle și top management în lanțuri de restaurante renumite din Irlanda și România.</p>
                        <p>Cu o pasiune pentru ospitalitate, Florin aduce experiența sa diversificată și profesionalismul în fiecare detaliu al restaurantului.</p>
                    </div>
                </div>
            </container>
            <container>
                <div className='container mx-auto flex flex-col justify-center text-center align-center min-h-[200px] space-y-5'>
                    <h2 className='uppercase text-5xl font-bold'>PREGĂTIȚI PENTRU <br></br>EVENIMENTUL TĂU</h2>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Ajustat Pentru Orice Buget</h3>
                        <p>La Piatto 77, ne dedicăm să vă oferim o experiență de neuitat, într-un cadru elegant și modern, indiferent de dimensiunea bugetului dumneavoastră.</p>
                        <p>Fie că este vorba despre o petrecere corporate intimă sau un eveniment grandios, echipa noastră vă va sprijini pentru a vă asigura că fiecare detaliu contribuie la o experiență memorabilă.</p>
                    </div>
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/buget-ajustat.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="burger la piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/meniu-personalizat.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="burger la piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Un Meniu Personalizat Pentru Orice Gust</h3>
                        <p>Suntem pregătiți să răspundem tuturor necesităților alimentare, inclusiv opțiuni pentru vegetarieni, vegani, și diete keto, asigurându-ne că experiența culinară este adaptată în totalitate la așteptările dumneavoastră.</p>
                        <p>Flexibilitatea noastră în personalizare ne permite să oferim soluții culinare variate, transformând orice eveniment într-o experiență gastronomică autentică și satisfăcătoare pentru toți participanții.</p>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col-reverse h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Experiență Fără Griji: <br />Noi Ne Ocupăm de Tot</h3>
                        <p>Petrecerea dvs. la Piatto 77 înseamnă mai mult decât organizarea unui eveniment.</p>
                        <p>Ne implicăm cu pasiune asigurându-ne că fiecare detaliu este perfect pus la punct.</p>
                        <p>De la primul telefon până la momentul final al evenimentului, vă garantăm că suntem alături de dumneavoastră, angajați să transformăm fiecare detaliu într-o amintire deosebită.</p>
                    </div>
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/fara-griji.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="ospatar cu bauturi la piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                </div>
            </container>
            <container>
                <div className="container mx-auto flex md:flex-row flex-col h-1/2 min-h-[300px]">
                    <div className="md:w-1/2 w-full align-center text-center flex justify-center p-10 rounder-xl">
                        <div className='relative  min-w-full min-h-[300px]'>
                            <Image src="/evenimente/contra-oferte.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="meniu pe masa la piatto 77" className='rounded-xl'></Image>
                        </div>
                    </div>
                    <div className="md:w-1/2 md:py-20 py-10 lg:px-24 md:px-12 px-10 space-y-3">
                        <h3 className='text-xl font-semibold pb-5'>Oferim Contra-Oferte​</h3>
                        <p>Dacă te afli în faza de evaluare a ofertelor și încă nu ai luat o decizie, trimite-ne cea mai bună ofertă pe care o ai, iar noi ne angajăm să îți prezentăm o propunere mai atractivă.</p>
                        <p>Suntem aici pentru a asigura că obții cea mai bună valoare pentru evenimentul tău și să întrecem așteptările tale.</p>
                    </div>
                </div>
            </container>

            <container>
                <div className='container mx-auto flex lg:flex-row flex-col min-h-[300px]'>
                    <div className='md:w-1/2  p-10 lg:p-20'>
                        <h2 className='text-2xl font-bold'>Executive Summary</h2>
                        <ul className='list-disc pl-[revert]'>
                            <li>Zona P-ța Romană</li>
                            <li>50-70 de persoane</li>
                            <li>Orice tip de evenimente: private, executive lounge,  etc</li>
                            <li>Combinația perfectă între ambient, staff și mâncare.</li>
                        </ul>
                    </div>
                    <div id="form">
                        <EventForm ></EventForm>
                    </div>
                </div>
            </container>
            <container>
                <div className='container mx-auto  text-center pt-10 pb-5s w-fill min-h-screen'>
                    <div className='align-center pb-10'>
                        <h2 className='text-4xl font-bold align-center'>Wall of Fame</h2>
                    </div>
                    <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 mx-5'>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-1.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-2.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-3.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-4.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-5.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-6.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-7.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-8.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-9.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-10.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-11.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-12.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-13.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='col-span-1 relative mx-5 min-w-full min-h-[300px]'>
                            <Image src="/evenimente/wall-of-fame/party-14.jpg" sizes='100vw' fill={true} style={{ objectFit: "cover" }} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                        </div>
                        <div className='bg-orange-400 w-full m-10 py-10'>
                            <h3 className='text-4xl font-semibold p-10 text-white'>Hai și tu la <br></br>
                                PIATTO 77</h3>

                            <Link href="#form">
                                <button className='bg-white text-black w-[200px] h-[50px] rounded-2xl align-center text-center'>Către Formular</button>
                            </Link>
                        </div>
                    </div>


                </div>
            </container>
        </>
    )
}

export default EvenimenteCorporate;