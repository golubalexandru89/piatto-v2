function PoliticaConfidentialitate() {
    return (
        <div className="container m-5">

            <h1 className="text-lg font-medium"><strong>POLITICA DE CONFIDENȚIALITATE</strong></h1>



            <p>Respectarea dreptului la protecția datelor cu caracter personal, precum și a dreptului la viață privată este una din misiunile fundamentale al restaurantului<strong> Piatto 77</strong>.</p>
            <p>Având in vedere modificările legislative la nivelul Uniunii Europene prin Regulamentul (UE) 2016/679 privind protecția persoanelor fizice în ceea ce privește prelucrarea datelor cu caracter personal și libera circulație a acestor date și de a abrogare a Directivei 95/46/CE (“GDPR”) care devine aplicabil începând cu data de 25 mai 2018, dorim să ne asigurăm ca drepturile dvs. sunt protejate și că aveți cunoștință de modalitatea prin care Piatto 77 prelucrează datele dvs. personale.</p>
            <p>Datele personale înseamnă orice informații privind o persoană fizică identificată sau identificabilă („persoana vizată”); o persoană fizică identificabilă este o persoană care poate fi identificată, direct sau indirect, în special prin referire la un element de identificare, cum ar fi un nume, un număr de identificare, date de localizare, un identificator online, sau la unul sau mai multe elemente specifice, proprii identității sale fizice, fiziologice, genetice, psihice, economice, culturale sau sociale.</p>
            <p>&nbsp;</p>
            <p>Piatto 77 est administrat de către următorul operator:</p>
            
            <p><strong>SC BRATANII FOOD SRL</strong> - J40/9607/2022 , RO46168879, cu sediul în Sediul in Bucuresti, str. Popa Tatu 1-3, Parter, Camera 1, Ap. 1, Sectorul 1</p>
            <p><strong>Punct de Lucru: </strong>Strada Polonă 77, Sector 2, București.</p>
            
            <p>&nbsp;</p>
            <ul className="list-disc">
                <li>Ce fel de date personale prelucrăm?</li>
                <li>Date personale ale minorilor</li>
                <li>Date sensibile</li>
                <li>În ce scop și în ce temei prelucrăm datele dvs. personale?</li>
                <li>Către ce persoane transmitem datele dvs. personale?</li>
                <li>Colectăm date personale de la terți?</li>
            </ul>
            <p>&nbsp;</p>
            <ul className="list-disc">
                <li>Transferăm datele dvs. în afara UE/SEE?</li>
                <li>Furnizarea de către dvs. a datelor personale ale altor persoane fizice</li>
            </ul>
            <p>Cât timp păstrăm datele dvs. personale?</p>
            <ul className="list-disc">
                <li>Care sunt drepturile dvs?</li>
                <li>Sunt datele dvs. în siguranță?</li>
                <li>Link-uri către alte site-uri web</li>
                <li>Întrebări sau reclamații</li>
                <li>Modificări ale politicii</li>
            </ul>


            <h2 className="text-lg font-medium">CE FEL DE DATE PERSONALE PRELUCRĂM?</h2>
            <p><strong>Dacă sunteți client sau potențial client: </strong></p>
            <p>Colectăm date cu caracter personal în urma celor mai multe interacțiuni cu dvs., precum și în cadrul celorlalte aspecte ale activității noastre. Categoriile de date personale pe care le colectăm sunt:</p>
            <ul className="list-disc">
                <li>a) numele si prenumele, adresa de e-mail, numărul de telefon și adresa dvs.;</li>
                <li>b) informațiile pe care le furnizați cu privire la preferințele dvs. de marketing sau în cursul participării la concursuri, tombole sau anumite oferte promoționale;</li>
                <li>c) datele personale furnizate de dvs. în vederea înscrierii într-un program dedicat membrilor noștri și/sau într-un program de fidelitate;</li>
                <li>d) recenzii și opinii referitoare la serviciile noastre;</li>
                <li>e) orice alte tipuri de informații pe care alegeți să ni le furnizați.</li>
            </ul>
            <p>&nbsp;</p>
            <p>De asemenea, camerele de supraveghere și alte măsuri de securitate aflate pe proprietățile noastre pot capta sau înregistra imagini ale oaspeților în locuri publice (cum ar fi intrarea în restaurant, holuri), precum și datele dvs. de localizare (prin imaginile surprinse de camerele de supraveghere, cardurile de acces și alte tehnologii).</p>
            <p>Puteți alege oricând ce date personale doriți să ne furnizați. Cu toate acestea, dacă alegeți să nu furnizați anumite date personale, în cazul în care temeiul solicitării noastre este respectarea unei obligații contractuale sau obligații necesare pentru încheierea unui contract, vom fi în imposibilitatea de a vă oferi anumite servicii, de exemplu: dacă nu doriți să ne dați nume, prenume, adresă de e-mail sau număr de telefon în cazul în care doriți sa faceți o rezervare sau o comandă, nu vom putea să efectuăm rezervarea sau comanda.</p>
            <p><strong>&nbsp;</strong></p>
            <p><strong>Dacă sunteți membru al programului de fidelizare Piatto 77</strong><strong>:</strong></p>
            <p>De asemenea, mai prelucrăm și datele obținute prin intermediul programului de fidelizare. Pentru mai multe detalii legate datele prelucrate, modalitatea, scopurile și temeiurile prelucrării, vă rugăm să consultați politica de confidențialitate prezentată în aplicația Piatto 77.</p>
            <p>Dacă nu sunteți membru, vă invităm să citiți mai multe pe site-ul <strong>www.piatto77.ro</strong>.</p>
            <p>&nbsp;</p>
            <p><strong>Dacă sunteți potențial angajat:</strong></p>
            <p>&nbsp;</p>
            <p>Colectăm informațiile din CV-ul trimis de dvs. precum și orice altă informație transmisă odată cu CV-ul.</p>
            <p><strong>Dacă sunteți utilizator al site-ului nostru www.piatto77.ro:</strong></p>
            <p>Simpla accesare a site-ului nostru nu va conduce la colectarea datelor dvs. personale cu excepția cazului în care dvs. introduceți de bunăvoie anumite date (de exemplu rezervări online, aplicații posturi vacante, anumite cereri/solicitări, etc).</p>
            <p>Totuși, colectăm ora și data accesării site-ului Internet și adresa IP din care a fost accesat site-ul nostru de Internet.</p>
            <p><strong>Dacă sunteți reprezentant sau persoană de contact al furnizorilor sau partenerilor noștri de afaceri:</strong></p>
            <p>Colectăm numele, prenumele, funcția precum și orice alte date furnizate de dvs. sau de societatea pe care o reprezentanți.</p>
            <p><strong>Dacă sunteți angajat:</strong></p>
            <p>Vă rugăm să vedeți politica de confidențialitate aferentă angajaților, adusă la cunoștință în momentul angajării și disponibilă oricând la departamentul de resurse umane.</p>


            <h2 className="text-lg font-medium">DATE PERSONALE ALE MINORILOR</h2>
            <p>Nu prelucrăm datele personale ale minorilor sub 16 ani.</p>


            <h2 className="text-lg font-medium">DATE SENSIBILE</h2>
            <p>Termenul „date sensibile“ se referă la originea rasială sau etnică, opiniile politice, confesiunea religioasă sau convingerile filozofice sau apartenența la sindicate și prelucrarea de date genetice, de date biometrice, de date privind sănătatea sau de date privind viața sexuală sau orientarea sexuală.</p>
            <p>În general, nu colectăm informații sensibile decât dacă doriți dvs. să ni le oferiți. Putem folosi datele privind sănătatea furnizate de dvs. pentru a vă servi mai bine și a vă îndeplini nevoile speciale (de exemplu, pregătirii mâncării fără includerea unor alimente la care sunteți alergic, furnizarea accesului pentru persoanele cu dizabilități).</p>


            <h2 className="text-lg font-medium">ÎN CE SCOP ȘI ÎN CE TEMEI PRELUCRĂM DATELE DVS PERSONALE?</h2>
            <p><strong>Dacă sunteți client </strong></p>
            <ul className="list-disc">
                <li><em>a) Rezervări restaurant sau în cazul solicitărilor de oferte sau de rezervare transmise de dvs. pentru anumite evenimente organizate sau care pot fi organizate </em></li>
            </ul>
            <p><em>Scop: </em>prelucrăm datele dvs. personale (i) pentru a vă rezerva un loc în cadrul restaurantului si (ii) pentru a răspunde solicitărilor de oferte sau de rezervare transmise de dvs.</p>
            <p><em>Temei: </em>încheierea unui contract</p>
            <ul className="list-disc">
                <li><em>b) Rezerv</em><em>ă</em><em>ri la bowling și biliard</em></li>
            </ul>
            <p><em>Scop: </em>prelucrăm datele dvs. personale (i) pentru a vă rezerva o pistă de bowling sau o masa de biliard și (ii) pentru a răspunde solicitărilor de oferte sau de rezervare transmise de dvs.</p>
            <p><em>Temei: </em>încheierea unui contract</p>
            <ul className="list-disc">
                <li><em>c) Comenzi cu ridicare sau livrare </em></li>
            </ul>
            <p><em>Scop: </em>prelucrăm datele dvs. personale pentru a putea îndeplini comenzile efectuate de dvs. indiferent dacă optați pentru a le servi în locație, sau pentru ridicare personală sau livrare.</p>
            <p><em>Temei: </em>executarea contractului de prestări servicii de restaurant</p>
            <ul className="list-disc">
                <li><em>d) Feedback: </em></li>
            </ul>
            <p><em>Scop: </em>prelucrăm datele dvs. personale pentru a ne asigura ca v-ați simțit bine la noi.</p>
            <p><em>Temei: </em>interesul nostru legitim de a îmbunătăți constant serviciile pe care le oferim și de a oferi servicii cât mai potrivite si conforme cu standardele clienților noștri</p>
            <ul className="list-disc">
                <li><em>e) Marketing: </em></li>
            </ul>
            <p><em>Scop: </em>prelucram datele dvs. personale în scopuri de marketing, precum newsletter-uri comerciale și comunicări de marketing privind produse și servicii noi sau alte oferte despre care credem că ar putea fi de interes pentru dvs.</p>
            <p><em>&nbsp;</em></p>
            <p><em>Temei: </em>ne bazăm pe interesul legitim de a promova serviciile noastre prin transmiterea ofertelor pe care le considerăm ca fiind de interes pentru dvs. (a se vedea “Dreptul la opoziție” din Secțiunea “Drepturile dumneavoastră”).</p>
            <p>Dacă este necesar, în conformitate cu legislația aplicabilă, vom obține consimțământul dvs. înainte de a vă prelucra datele personale în scopuri de marketing direct. În acest caz, vă aducem la cunoștință că veți putea retrage în orice moment consimțământul pentru prelucrarea în scop de marketing, caz în care nu veți mai primi nicio comunicare de marketing din partea noastră.</p>
            <p>Vom include un link de dezabonare pe care îl puteți utiliza dacă nu doriți să vă mai trimitem mesaje.</p>
            <p>De asemenea, cu ocazia organizării anumitor evenimente în cadrul restaurantelor noastre, este posibil să realizăm câteva fotografii, iar unele dintre aceste ar putea fi distribuite și în mediul online pentru a arăta și celorlalți cum sunt evenimentele noastre. În orice caz, pentru că ținem cont de dreptul la viață privată, avem grijă ca fotografiile nu să evidențieze anumite persoane si vom face tot posibilul să fiți informați despre faptul că vor fi făcute fotografii (pentru obiecțiuni cu privire la fotografiile noastre a se vedea “Dreptul la opoziție” din Secțiunea “Drepturile dumneavoastră”).</p>
            <ul className="list-disc">
                <li><em>f) Alte comunicări</em>: prin e-mail, poștă, telefon sau SMS</li>
            </ul>
            <p><em>Scop: </em>aceste comunicări vor fi făcute pentru un motiv specific precum: (i) pentru a răspunde cererilor dumneavoastră, (ii) dacă nu ați finalizat o rezervare online, este posibil să vă trimitem un e-mail pentru a vă reaminti să finalizați rezervarea, (iii) pentru a va informa despre modalitatea în care au fost soluționate plângerile și/sau incidentele apărute în perioada petrecută în restaurantul nostru.</p>
            <p><em>Temei juridic: </em>interesul nostru legitim de a oferi servicii la cele mai înalte standarde prin soluționarea eventualelor cereri/plângeri și pentru a vă asigura de întreaga noastră disponibilitate.</p>
            <ul className="list-disc">
                <li><em>g) Analiză, îmbunătățire și cercetare: </em></li>
            </ul>
            <p>Scop: pentru a asigura constant evoluția calitativă a serviciilor noastre, avem grijă să analizăm fiecare plângere/sugestie din partea dvs., astfel că întocmim rapoarte statistice pentru a identifica problemele, gradul de repetitivitate și pentru a găsi cele mai bune soluții în vederea remedierii acestora.</p>
            <p>Temei: ne bazăm pe interesul nostru legitim de a furniza servicii conforme standardelor dvs. și ale Piatto 77.</p>
            <p>&nbsp;</p>
            <p><strong>Dacă sunteți utilizator al site-ului nostru de internet www.piatto77.ro </strong></p>
            <p><em>Scop</em>: monitorizarea traficului în vederea identificării erorilor si/sau orice altă disfuncționalitate a site-ului</p>
            <p><em>Temei: </em>ne bazăm această activitate de prelucrare a datelor pe interesul nostru legitim și anume punerea la dispoziția dvs. a unui site complet funcțional prin repararea oricăror erori, precum și îmbunătățirea continuă a acestuia.</p>
            <p><strong>Dacă sunteți reprezentant sau persoană de contact al furnizorilor sau partenerilor noștri de afaceri </strong></p>
            <p><em>Scop</em>: derularea relațiilor contractuale cu furnizorii sau partenerii noștri de afaceri.</p>
            <p><em>Temei: executarea unui contract. </em></p>
            <p><strong>Dacă sunteți potențial angajat </strong></p>
            <p>Scop: evaluarea aplicației dvs. de angajare.</p>
            <p>Temei: încheierea unui contract</p>
            <p><strong>Dacă sunteți angajat </strong></p>
            <p>Vă rugăm să vedeți politica de confidențialitate aferentă angajaților, adusă la cunoștință în momentul angajării și disponibilă oricând la departamentul de resurse umane.</p>
            <p>&nbsp;</p>
            <p><strong>Pentru toate categoriile de persoane de mai sus, </strong>putem procesa și datele dvs. în contextul următoarelor activități:</p>
            <ul className="list-disc">
                <li><em>a) Restructurărilor sau reorganizărilor interne sau al unor vânzări de active sau părți sociale/acțiuni: </em></li>
            </ul>
            <p>Scop: prelucrăm datele dvs. pentru efectuarea operațiunilor menționate mai sus</p>
            <p><em>Temei</em>: interesul legitim de a efectua operațiunile, mai ales în condițiile în care acestea ar fi imposibil de realizat în lipsa unei prelucrări a datelor dvs.</p>
            <p>Cu toate acestea, vă asigurăm că această eventuală prelucrare va fi efectuată în conformitate cu prezenta politică și prin implementarea unor măsuri care să asigure confidențialitatea datelor dvs.</p>
            <p>&nbsp;</p>
            <ul className="list-disc">
                <li><em>b) Securitate: </em></li>
            </ul>
            <p><em>Scop: </em>prelucrăm datele personale pentru securitatea bunurilor și integritatea fizică a persoanelor.</p>
            <p><em>Temei: </em>ne bazăm pe interesul nostru legitim de a asigura protecția bunurilor dvs., ale restaurantului, precum și protecția persoanelor.</p>
            <p>&nbsp;</p>
            <ul className="list-disc">
                <li>c) Motive juridice:</li>
            </ul>
            <p><em>Scop</em>: în anumite cazuri, trebuie să prelucrăm informațiile furnizate, care pot include date cu caracter personal, pentru a rezolva disputele juridice sau reclamațiile, pentru investigațiile și respectarea reglementărilor legale aplicabile, pentru a pune în aplicare un acord sau pentru a ne conforma solicitărilor din partea organelor publice în măsura în care aceste solicitări îndeplinesc condițiile impuse de lege.</p>
            <p><em>Temei</em>: motivele prelucrării pot fi reprezentate de obligația legală (în cazul în care avem obligația legală de a divulga anumite date cu caracter personal autorităților publice) sau</p>
            <p>de interesul nostru legitim de a rezolva eventualele dispute și/sau reclamații.</p>


            <h2 className="text-lg font-medium">CĂTRE CE PERSOANE TRANSMITEM DATELE DVS. PERSONALE?</h2>
            <p>Pentru a vă oferi nivelul așteptat de ospitalitate și a vă furniza servicii de înaltă calitate, este posibil să transmitem informațiile dvs. personale în restaurantul noastru (Piatto 77), furnizorilor noștri de servicii și altor terțe părți, după cum este prezentat în detaliu mai jos:</p>
            <ul className="list-disc">
                <li>a) <em>Furnizori: </em>în vederea furnizării serviciilor solicitate, în unele cazuri, va trebui să transmitem câteva din datele dvs. personale furnizorilor, iar aceștia au calitatea de împuterniciți și prelucrează datele în numele, pe seama și în conformitate cu indicațiile noastre (cum ar fi furnizorii de software-uri, IT, servicii de contabilitate, agenții de marketing, servicii medicale, payroll).</li>
                <li>b) <em>Restaurant: </em>pentru scopuri similare sau în legătură cu cele pentru care acestea au fost colectate.</li>
                <li>c) <em>Evenimente</em>: dacă participați la anumite evenimente în cadrul restaurantului, iar acest eveniment este organizat în colaborare cu terți, datele dvs. personale acordate în vederea participării la evenimente pot fi partajate cu ceilalți organizatori și, dacă este cazul, cu alți participanți la eveniment.</li>
                <li>d) <em>Parteneri de afaceri: </em>în anumite cazuri, ne asociem cu alte societăți pentru a vă furniza produse, servicii sau oferte (spre exemplu parteneriatul cu Mastercard care presupune acordarea de reduceri la nota de plată a clienților care achită cu un card bancar Mastercard).</li>
                <li>e) <em>Co-sponsori ai promoțiilor</em>: în anumite cazuri, co-sponsorizam promoții, loterii, tombole, competiții sau concursuri cu alte companii sau putem furniza premii pentru loterii și concursuri sponsorizate de alte companii. Dacă participați la aceste loterii sau concursuri, putem transmite datele dvs.. personale sponsorul colaborator sau unui sponsor terț.</li>
            </ul>
            <p>&nbsp;</p>
            <p>Autorități și/sau instituții publice pentru: (i) respectarea dispozițiilor legale, (ii) a răspunde solicitărilor acestora, (iii) pentru motive de interes public (ex: siguranță națională).</p>
            <p>Confidențialitatea datelor dvs. este importantă pentru noi, motiv pentru care, acolo unde este posibil, transmiterea de date personale în conformitate cu cele de mai sus se realizează numai în temeiul unui angajament de confidențialitate din partea destinatarilor, prin care garantează că aceste date sunt păstrate în siguranță și că furnizarea acestor informații se face conform legislației în vigoare și politicilor aplicabile. În orice caz, de fiecare dată vom transmite către destinatari doar informațiile strict necesare pentru realizarea scopului respectiv.</p>


            <h2 className="text-lg font-medium">COLECTĂM DATE PERSONALE DE LA TERȚI?</h2>
            <p>Pentru a vă oferi nivelul așteptat de ospitalitate și a vă furniza cel mai bun nivel al serviciilor, este posibil ca datele personale furnizate restaurantului Piatto 77 să fie transmise și către partenerii noștri de afaceri și alte terțe părți, după cum este prezentat în detaliu mai jos:</p>
            <ul className="list-disc">
                <li>a) <em>Restaurant: </em>pentru scopuri similare sau în legătură cu cele pentru care acestea au fost colectate.</li>
            </ul>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <ul className="list-disc">
                <li>b) Parteneri <em>precum: BestJobs, publi24, OLX, licee profesionale si scoli vocationale, LinkedIn, alti furnizori de formare profesionala în vederea angajării de personal. </em></li>
                <li>c) <em>Co-sponsori ai promoțiilor</em>: în anumite cazuri, co-sponsorizam promoții, loterii, tombole, competiții sau concursuri cu alte companii sau putem furniza premii pentru loterii și concursuri sponsorizate de alte companii. Dacă participați la loterii sau concursuri, putem colecta datele dvs. personale de la sponsorul colaborator sau de la un sponsor terț în vederea organizării acestora și pentru acordarea premiilor.</li>
            </ul>
            <p>&nbsp;</p>
            <p>În orice caz, vă asigurăm ca datele dvs. colectate de la terți vor fi prelucrate în aceleași condiții ca și cum ar fi fost colectate direct de la dvs. De asemenea, nu vom colecta decât ceea ce este necesar îndeplinirii scopurilor noastre (a se vedea Secțiunea “IN CE SCOP ȘI ÎN CE TEMEI SUNT PRELUCRATE DATELE DVS PERSONALE?”).</p>
            <p>În plus, în momentul în care vă vom contacta pentru prima data, vă vom aduce la cunoștință, în primul rând, sursa din care am obținut datele dvs. personale.</p>


            <h2 className="text-lg font-medium">TRANSFERĂM DATELE DVS PERSONALE ÎN AFARA UE/SEE?</h2>
            <p>Nu, datele dvs. nu vor fi transmise către țări care nu fac parte de Uniunea Europeana sau Spațiul Economic European.</p>


            <h2 className="text-lg font-medium">FURNIZAREA DE CĂTRE DVS. A DATELOR PERSONALE ALE ALTOR PERSOANE FIZICE</h2>
            <p>Dacă ne furnizați datele personale ale altor persoane fizice, vă rugăm să le comunicați înainte de această dezvăluire și modalitatea în care acestea urmează a fi prelucrate, așa cum este descris în această politică de confidențialitate.</p>


            <h2 className="text-lg font-medium">CÂT TIMP PĂSTRĂM DATELE DVS PERSONALE?</h2>
            <p>Datele dvs. personale sunt păstrate pe toată perioada realizării scopurilor detaliate în prezenta politică, dacă o perioadă mai lungă de timp de păstrare nu este necesară sau permisă de legea aplicabilă (spre exemplu înregistrările camerelor de supraveghere nu vor fi păstrate mai mult de 30 de zile).<br />
                Revizuim constant necesitatea păstrării datelor dvs. personale, iar în măsura în care prelucrarea nu mai este necesară și nu există obligativitatea impusă de lege de a păstra datele , vom distruge datele dvs. personale cât mai curând posibil și într-un mod în care să nu mai poată fi recuperate sau reconstituite (spre exemplu, vom șterge/distruge toate datele persoanelor care nu au fost angajate în urma interviurilor, dacă nu există perspectiva serioasă de angajare în viitor).<br />
                Dacă informațiile personale sunt imprimate pe suport de hârtie, acestea vor fi distruse într-un mod sigur, prin shredder, iar dacă acestea vor fi salvate pe suporturi electronice, vor fi șterse prin mijloace tehnice pentru a asigura faptul că informațiile nu mai pot fi recuperate sau reconstituite ulterior.</p>


            <h2 className="text-lg font-medium">CARE SUNT DREPTURILE DUMNEAVOASTRĂ?</h2>
            <p>Î</p>
            <p>În calitate de persoana vizată beneficiați de următoarele drepturi prevăzute de GDPR:</p>
            <ul className="list-disc">
                <li>a) <em>Dreptul de acces</em>: ne puteți solicita (i) o confirmare a faptului că se prelucrează sau nu date cu caracter personal și, în caz afirmativ, acces la datele respective și informații cu privire la acestea, precum și (ii) o copie a datelor personale pe care le deținem (art. 15 din GDPR);</li>
                <li>b) <em>Dreptul la rectificare: </em>ne puteți informa despre orice modificare a datelor dvs. personale sau ne puteți cere să corectăm datele personale pe care le deținem despre dvs. (art. 16 din GDPR);</li>
                <li>c) <em>Dreptul la ștergere („dreptul de a fi uitat”): </em>în anumite situații (ca de exemplu (i) cazul in care datele au fost colectate ilegal, (ii) data limita pentru stocarea datelor a expirat, (iii) v-ați exercitat dreptul la opoziție sau (iv) prelucrarea datelor se realiza pe bază de consimțământ și v-ați retras consimțământul), ne puteți cere să ștergem datele personale pe care le deținem despre dvs. (art. 17 din GDPR);</li>
                <li>d) <em>Dreptul de a restricționa prelucrarea: </em>în anumite situații (ca de exemplu cazul în care este contestați exactitatea acestor date sau legalitatea prelucrării), ne puteți cere restricționăm prelucrarea datelor dvs. pentru o anumită perioadă (art. 18 din GDPR);</li>
                <li>e) <em>Dreptul la portabilitatea datelor</em>: să ne solicitați să vă trimitem datele dvs. personale unei terțe părți sau direct către dvs. (art. 20 din GDPR);</li>
                <li>f) <em>Dreptul la opoziție</em>: în anumite situații (ca de exemplu prelucrarea ce are ca temei juridic interesul legitim), ne puteți solicita să nu mai prelucrăm datele dvs. (art. 21 din GDPR);</li>
            </ul>
            <p>&nbsp;</p>
            <p>În cazul în care folosim datele dvs. personale pe baza consimțământului dvs., aveți dreptul să retrageți acest consimțământ în orice moment. În această situație, datele dvs. nu vor mai fi prelucrate de noi, cu excepția cazului în care o dispoziție legală ne obligă la păstrarea și arhivarea acestora. In orice caz, vă vom informa dacă există o astfel de prevedere legală și o vom indica în mod expres.</p>
            <p>Pentru a afla cum vă puteți exercita oricare dintre drepturile de mai sus, vă rugăm să consultați secțiunea „<strong>Întrebări sau reclamații”</strong></p>


            <h2 className="text-lg font-medium">SUNT DATELE DVS. ÎN SIGURANȚĂ?</h2>
            <p>Tratăm securitatea dvs. personale în mod foarte serios, de aceea, luăm importante măsuri de securitate, necesare pentru protecția împotriva accesului neautorizat la date sau modificării, dezvăluirii ori distrugerii neautorizate de date. Acest lucru presupune revizuiri interne ale practicilor de colectare, păstrare și procesare de date și ale măsurilor de securitate, de asemenea, măsuri fizice de securitate pentru protecția împotriva accesului neautorizat la sistemele unde stocăm datele personale.<br />
                Solicităm și furnizorilor noștri de servicii precum și partenerilor de afaceri să întreprindă toate măsurile necesare în vederea protecției împotriva accesului neautorizat la date sau modificării, dezvăluirii ori distrugerii neautorizate de date.</p>


            <h2 className="text-lg font-medium">LINKURI CĂTRE ALTE SITE-URI WEB</h2>
            <p>Site-ul nostru conține linkuri către site-uri terțe. Rețineți faptul că nu ne asumăm responsabilitatea pentru colectarea, utilizarea, păstrarea, partajarea sau dezvăluirea datelor sau informațiilor de către astfel de terțe părți. În cazul în care folosiți sau furnizați informații pe site-urile terților, se aplică termenii și politica de confidențialitate a site-urilor respective. Vă sfătuim să citiți politica de confidențialitate ale site-urilor pe care le vizitați înainte de a trimite date personale.<br />
                Folosirea serviciilor de internet oferite de restaurantele noastre cade sub incidența termenilor de utilizare și a politicii de confidențialitate ale furnizorilor de internet. Puteți accesa termenii și politicile respective utilizând linkurile de pe pagina de autentificare a serviciului respectiv sau vizitând site-ul web al furnizorului de internet.</p>


            <h2 className="text-lg font-medium">ÎNTREBĂRI SAU RECLAMAȚII</h2>
            <p>Dacă aveți întrebări sau preocupări legate de prelucrarea datelor dvs. personale sau dacă doriți să vă exercitați oricare dintre drepturile menționate mai sus, sunteti binevenit(ă) să ne contactați în scris la următoarea adresă de e-mail: marketing@piatto77.ro.<br />
                De asemenea, în măsura în care nu dispuneți de mijloace electronice sau nu doriți să le folosiți, puteți formula o cerere scrisă pe care o depuneți la adresa str. Polonă 77, sector 2, București.<br />
                Pentru orice alte informații suplimentare, ne puteți contacta la adresa de e-mail marketing@piatto77.ro.<br />
                Ulterior transmiterii cererii, un reprezentant al Piatto 77 va lua legătura cu dvs. și va solicita informații suplimentare pentru identificarea dvs. și facilitarea exercitării dreptului. Este important să dați curs solicitării reprezentantului Piatto 77 și să furnizați toate informațiile necesare pentru a vă putea identifica, în caz contrar, din motive de securitate, vom fi nevoiți să respingem cererea dvs.<br />
                În orice caz, vom răspunde în cel mult 30 de zile de la primirea cererii.<br />
                În măsura în care nu sunteți satisfăcut cu modalitatea în care v-a fost soluționată cererea, puteți depune o reclamație la Autoritatea Națională de Supraveghere a Prelucrării Datelor cu Caracter Personal.</p>


            <h2 className="text-lg font-medium">MODIFICĂRI ALE POLITICII</h2>
            <p>Această politică de confidențialitate este supusă actualizărilor periodice în funcție de modificările apărute în reglementările europene sau naționale privind protecția datelor cu caracter personal. Dacă vom efectua modificări materiale la aceasta, vom publica un link către politica revizuită pe pagina de pornire a site-ului nostru. Dacă vom efectua modificări semnificative care vor avea un impact asupra drepturilor și libertăților dvs. (de exemplu, atunci când vom începe prelucrarea datelor dvs. personale în alte scopuri decât cele specificate mai sus), vă vom contacta înainte de a începe această prelucrare.<br />
                Pentru a vă ajuta să urmăriți modificările cele mai importante, vom include mai jos un istoric al modificărilor pentru a recunoaște modificările aduse Politicii de confidențialitate.<br />
                Ultima actualizare: 12.12.2022</p>


        </div>

    )
}
export default PoliticaConfidentialitate