function PoliticaCookies() {
    return (
        <div className="container m-5">

            <h2 className="text-lg font-medium"><strong>1. Introducere</strong></h2>
            <p>Prezenta Politică privind fișierele cookies se aplică tuturor utilizatorilor paginii de internet www.piatto77.ro. Informațiile prezentate în continuare au ca scop informarea utilizatorilor acestei pagini de internet cu privire la plasarea, utilizarea și administrarea cookie-urilor de către S.C. BRATANII FOOD SRL în contextul navigării utilizatorilor pe această pagină de internet.</p>
            <h2 className="text-lg font-medium">2. Ce sunt cookie-urile?</h2>
            <p>Folosim termenul „cookie”-uri pentru a ne referi la modulele cookie și la tehnologiile similare prin intermediul cărora pot fi colectate informații în mod automat.<br/>
                Un „Internet Cookie” (termen cunoscut și sub denumirea de „browser cookie” sau „HTTP cookie” ori „cookie”) reprezintă un fișier de mici dimensiuni, format din litere și numere, care va fi stocat pe computerul, terminalul mobil sau pe alte echipamente ale unui utilizator prin intermediul cărora se accesează internetul.<br/>
                    Cookie-urile sunt instalate prin solicitarea emisă de un web-server către un browser (de ex.: Internet Explorer, Firefox, Chrome). Cookie-urile odată instalate au o durată de existență determinată, rămânând „pasive”, în sensul că nu conțin programe software, viruși sau spyware și nu vor accesa informațiile de pe hard driverul utilizatorului pe al cărui echipament au fost instalate.<br/>
                        Un cookie este format din două părți: numele cookie-ului; și conținutul sau valoarea cookie-ului.<br/>
                            Din punct de vedere tehnic, doar web-serverul care a trimis cookie-ul îl poate accesa din nou în momentul în care un utilizator se întoarce pe pagina de internet asociată web-serverului respectiv.</p>
                        <h2 className="text-lg font-medium"><strong>3. Pentru ce scopuri sunt utilizate cookie-urile prin intermediul acestei pagini de internet:</strong></h2>
                        <p>Cookie-urile sunt utilizate pentru a furniza utilizatorilor acestei pagini de internet o experiență mai bună de navigare și servicii adaptate nevoilor și interesului fiecărui utilizator în parte și anume pentru:</p>
                        <ul className="list-disc">
                            <li>îmbunătățirea utilizării acestei pagini de internet, inclusiv prin identificarea oricăror erori care apar în timpul vizitării/utilizării acesteia de către utilizatori;</li>
                            <li>furnizarea de statistici anonime cu privire la modul în care este utilizată această pagină de internet către S.C. BRATANII FOOD SRL, în calitate de deținători al acestei pagini de internet;</li>
                            <li>anticiparea unor eventuale bunuri care vor fi în viitor puse la dispoziția utilizatorilor prin intermediul acestei pagini de internet, în funcție de serviciile / produsele accesate.</li>
                        </ul>
                        <p>Pe baza feedback-ului transmis prin cookie-uri în legătură cu modul în care se utilizează această pagină de internet, S.C. BRATANII FOOD SRL pot adopta măsuri pentru ca această pagină de internet să fie mai eficientă și mai accesibilă pentru utilizatori.<br/>
                            Astfel, utilizarea cookie-urilor permite memorarea anumitor setări/preferințe stabilite de către utilizatorii acestei pagini de internet, precum:</p>
                        <ul className="list-disc">
                            <li>limba în care este vizualizată o pagină de internet;</li>
                            <li>moneda în care se exprimă anumite prețuri sau tarife;</li>
                        </ul>
                        <h2 className="text-lg font-medium"><strong>4. Care este durata de viață a cookie-urilor?</strong></h2>
                        <p>Durata de viață a cookie-urilor poate varia semnificativ, depinzând de scopul pentru care este plasat. Există următoarele categorii de cookie-uri care determină și durata de viață a acestora:</p>
                        <ul className="list-disc">
                            <li><strong>Cookie-uri de sesiune</strong>&nbsp;– Un „cookie de sesiune” este un cookie care este șters automat când utilizatorul își închide browserul.</li>
                            <li><strong>Cookie-uri persistente sau fixe</strong>&nbsp;– Un „cookie persistent” sau „fix” este un cookie care rămâne stocat în terminalul utilizatorului până când atinge o anumită dată de expirare (care poate fi în câteva minute, zile sau câțiva ani în viitor) sau până la ștegerea acestuia de către utilizator în orice moment prin intermediul setărilor browserului.</li>
                        </ul>
                        <h2 className="text-lg font-medium"><strong>5. Ce sunt cookie-urile plasate de terți?</strong></h2>
                        <p>Anumite secțiuni de conținut de pe pagina de internet pot fi furnizate prin intermediul unor terți, adică nu de către S.C. BRATANII FOOD SRL, caz în care aceste cookie-uri sunt denumite cookie-uri plasate de terți („third party cookie-uri”).<br/>
                            Terții furnizori ai cookie-urilor trebuie să respecte, de asemenea, regulile în materie de protecție a datelor și&nbsp;<a href="https://piatto77.ro/politica-de-confidentialitate/">Politica de Confidențialitate</a>&nbsp; disponibilă pe această pagină de internet.<br/>
                                Aceste cookie-uri pot proveni de la urmatorii terți: Google Analytics, Facebook, Instagram, Linkedin, Trip Advisor.</p>
                            <h2 className="text-lg font-medium"><strong>6. Ce cookie-uri sunt folosite prin intermediul acestei pagini de internet:</strong></h2>
                            <p>Prin utilizarea/vizitarea paginii de internet pot fi plasate următoarele cookie-uri:</p>
                            <p>a. Cookie-uri de performanță a paginii de internet;<br/>
                                b. Cookie-uri de analiză a utilizatorilor;<br/>
                                    c. Cookie-uri pentru geotargetting;<br/>
                                        d. Cookie-uri pentru publicitate;<br/>
                                            e. Cookie-uri ale furnizorilor de publicitate;</p>
                                        <p><strong>a. Cookie-uri de performanță</strong></p>
                                        <p>Prin acest tip de cookie-uri sunt memorate preferințele utilizatorului acestei pagini de internet, astfel încât setarea din nou a preferințelor în cazul vizitării ulterioare a paginii de internet nu mai este necesară.</p>
                                        <p><strong>b. Cookie-uri de analiză a utilizatorilor</strong></p>
                                        <p>Aceste cookie-uri ne informează dacă un anumit utilizator al paginii de internet a mai vizitat/utilizat această pagină de internet anterior. Aceste cookie-uri sunt utilizate doar în scopuri statistice.</p>
                                        <p><strong>c. Cookie-uri pentru geotargetting</strong></p>
                                        <p>Aceste cookie-uri sunt utilizate de către un soft care stabilește țara de proveniență a utilizatorului paginii de internet. Vor fi primite aceleași reclame indiferent de limba selectată.</p>
                                        <p><strong>d. Cookie-uri pentru publicitate</strong></p>
                                        <p>Aceste cookie-uri permit aflarea vizualizării de către un utilizator a unei reclame online, tipul acesteia și timpul scurs de la momentul vizualizării respectviului mesaj publicitar. Ca atare, astfel de cookie-uri sunt folosite pentru targetarea publicității online. Aceste cookie-uri sunt anonime, stocând informații despre contentul vizualizat, nu și despre utilizatori.</p>
                                        <h2 className="text-lg font-medium"><strong>7. Ce tip de informații sunt stocate și accesate prin intermediul cookie-urilor?</strong></h2>
                                        <p>Cookie-urile păstrează informații într-un fișier text de mici dimensiuni care permit recunoașterea browserului. Această pagină de internet recunoaște browserul până când cookie-urile expiră sau sunt șterse.</p>
                                        <h2 className="text-lg font-medium"><strong>8. Particularizarea setările browserului în ceea ce privește cookie-urile</strong></h2>
                                        <p>În cazul în care utilizarea cookie-urilor nu este deranjantă iar calculatorul sau echipamentul tehnic utilizat pentru navigarea pe această pagină de internet este folosit doar de către dumneavoastră, pot fi setate termene lungi de expirare pentru stocrarea istoricului de navigare.<br/>
                                            În cazul în care calculatorul sau echipamentul tehnic utilizat pentru navigarea pe această pagină de internet este folosit de mai multe persoane, poate fi luată în considerare setarea pentru ștergerea datelor individuale de navigare de fiecare dată când browserul este închis.</p>
                                        <h2 className="text-lg font-medium"><strong>9. Cum pot fi oprite cookie-urile?</strong></h2>
                                        <p>Dezactivarea și refuzul de a primi cookie-uri pot face această pagină de internet dificil de vizitat, atrăgând după sine limitări ale posibilităților de utilizare ale acesteia.<br/>
                                            Utilizatorii își pot configura browserul să respingă fișierele cookie sau să fie acceptate cookie-uri de la o pagină de internet anume.<br/>
                                                Toate browserele moderne oferă posibilitatea de a schimba setările cookie-urilor. Aceste setări pot fi accesate, ca regulă, în secțiunea „opțiuni” sau în meniul de „preferințe” al browserului tău.<br/>
                                                    Totuși, refuzarea sau dezactivarea cookie-urilor nu înseamnă că nu veți mai primi publicitate online – ci doar ca aceasta nu va fi adaptată preferințelor și interesele dumneavoastră, evidențiate prin comportamentul de navigare.</p>
                                                <p>Pentru a înțelege aceste setări, următoarele linkuri pot fi folositoare:</p>
                                                <ul className="list-disc">
                                                    <li><a href="https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies">Cookie settings in Internet Explorer</a></li>
                                                    <li><a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences">Cookie settings in Firefox</a></li>
                                                    <li><a href="https://support.google.com/chrome/answer/95647">Cookie settings in Chrome</a></li>
                                                    <li><a href="https://support.apple.com/kb/ph21411">Cookie settings in Safari</a></li>
                                                </ul>
                                                <p>Pentru orice întrebări suplimentare cu privire la modul în sunt utilizate cookie-urile prin intermediul acestei pagini de internet, vă rugăm să vă adresați la: email: marketing@piatto77.ro sau numar de telefon: 0724.554.477</p>

                    </div>
                    )
}
                    export default PoliticaCookies