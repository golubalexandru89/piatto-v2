import Image from 'next/image'
import Link from 'next/link';

export const metadata = {
    title: "Despre Concept | PIATTO 77 - Urban Kitchen ",
    description: 'Bucătăria urbană a încorporat tehnici și retete din bucătăriile internaționale. Vino alături de noi pentru o experiență culinară cu ingrediente proaspete și sănătoase.',
    languages: "ro-Ro",
    openGraph: {
        title: "Despre Concept | PIATTO 77 - Urban Kitchen ",
        description: 'Bucătăria urbană a încorporat tehnici și retete din bucătăriile internaționale. Vino alături de noi pentru o experiență culinară cu ingrediente proaspete și sănătoase.',
        url: 'https://piatto77.ro/urban-kitchen',
        siteName: 'PIATTO 77 - Urban Kitchen',
        images: [
          {
            url: '/hero-image.jpg',
            width: 800,
            height: 600,
          },
        ],
        locale: 'ro_RO',
        type: 'website',
      }

}

function UrbanKitchen() {



    return (
        <>
            <div className="relative isolate p-4 md:px-4 lg:px-8">
                <div className="container mx-auto py-14 sm:py-48 lg:py-56 relative overflow-hidden border-2 rounded-xl">
                    <div className="bg-[url('/urban-kitchen/hero.jpg')] bg-center bg-cover h-full w-full absolute top-0 left-0"></div>
                    <div className="bg-black bg-opacity-10 backdrop-blur-sm	h-full w-full absolute top-0 left-0"></div>

                    <div className="text-left align-bottom md:w-2/3 w-full px-4 lg:px-20 md:pl-20 relative z-10">
                        <h1 className="md:text-[6rem] font-bold tracking-tight text-white text-[3.5rem] ">
                            <span className="text-transparent gradient-text animate-gradient">Urban Kitchen</span>
                        </h1>

                        <p className="mt-6 text-white text-xl">
                            Vino alături de noi pentru o experiență culinară care întruchipează principiile bucătăriei urbane.
                        </p>

                        <Link href="/meniu">
                            <button className="mt-8 bg-white rounded-full px-6 py-3 text-black">
                                Vezi Meniul
                            </button>
                        </Link>
                    </div>
                </div>
            </div>

            <div className="md:container mx-auto  flex md:flex-row flex-col-reverse md:h-1/2 min-h-[300px] pt-10">
                <div className="md:w-1/2  lg:py-20 py-5 lg:px-[12%] md:px-[5%] px-10 space-y-3">
                    <h3 className='text-xl font-semibold pb-5'>Originile Gătitului Urban Kitchen</h3>
                    <p>Bucătăria urbană a apărut în zonele urbane la începutul secolului al XX-lea. Bucătarii au folosit ingrediente de proveniență locală pentru a crea mâncăruri gustoase ca răspuns la industrializarea producției alimentare</p>

                </div>
                <div className="md:w-1/2  min-h-[250px] lg:min-h-[400px] align-center text-center flex justify-center p-10 rounder-xl relative mx-5">
                    <div className='mx-10'>
                        <Image src="/evenimente/originile-gatitului.jpg" fill={true} alt="origini gatit urban kitchen" className='rounded-xl'></Image>
                    </div>
                </div>
            </div>

            <div className="md:container mx-auto  flex md:flex-row flex-col md:h-1/2 min-h-[300px] pt-10">
                <div className="md:w-1/2  align-center text-center flex justify-center p-10 rounder-xl relative min-h-[250px] mx-5">
                    <div>
                        <Image src="/evenimente/evolutia-urban-kitchen.jpg" fill={true} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                    </div>
                </div>
                <div className="md:w-1/2  lg:py-20 py-5 lg:px-[12%] md:px-[5%] px-10 space-y-3">
                    <h3 className='text-xl font-semibold pb-5'>Evoluția pe parcursul timpului</h3>
                    <p>Bucătăria urbană a evoluat pentru a încorpora tehnici și ingrediente din bucătăriile internaționale.</p>
                    <p>Ea pune accentul pe ingrediente proaspete și sănătoase și pe metode de gătit inovatoare, ceea ce o face din ce în ce mai populară pentru cei care caută experiențe culinare unice și pline de savoare.</p>

                </div>
            </div>

            <div className="md:container mx-auto  flex md:flex-row flex-col-reverse md:h-1/2 min-h-[300px] pt-10">
                <div className="md:w-1/2  lg:py-20 py-5 lg:px-[12%] md:px-[5%] px-10 space-y-3">
                    <h3 className='text-xl font-semibold pb-5'>Influențe Importante</h3>
                    <p>Bucătarii de renume, printre care Jamie Oliver, Anthony Bourdain și Gordon Ramsay, au popularizat ingrediente proaspete, de proveniență locală și tehnici de gătit inovatoare din diferite bucătării internaționale.</p>
                    <p>Influența lor a ridicat bucătăria urbană la rang de fenomen global, inspirând bucătarii și iubitorii de mâncare deopotrivă să exploreze diverse arome.</p>
                </div>
                <div className="md:w-1/2 align-center relative text-center flex justify-center md:p-20 min-h-[250px] rounder-xl mx-5">
                    <Image src="/urban-kitchen/influente-internationale.jpg" fill={true} alt="origini gatit urban kitchen" className='rounded-xl'></Image>
                </div>
            </div>

            <div className="md:container mx-auto  flex md:flex-row flex-col md:h-1/2 min-h-[300px] pt-10">
                <div className="md:w-1/2  align-center text-center flex justify-center p-10 rounder-xl relative mx-5 min-h-[250px]">
                    <div>
                        <Image src="/urban-kitchen/experiente-unice.jpg" fill={true} alt="origini gatit urban kitchen" className='rounded-xl'></Image>
                    </div>
                </div>
                <div className="md:w-1/2  lg:py-20 py-5 lg:px-[12%] md:px-[5%] px-10 space-y-3">
                    <h3 className='text-xl font-semibold pb-5'>Experiențe Unice</h3>
                    <p>Cei care iau masa la noi se pot aștepta la o experiență culinară unică și memorabilă. </p>
                    <p>De la atmosfera caldă și primitoare până la mâncărurile inovatoare și gustoase, fiecare aspect al restaurantului este conceput pentru a oferi o experiență culinară unică.</p>

                </div>
            </div>

            <div className="md:container mx-auto  flex md:flex-row flex-col-reverse md:h-1/2 min-h-[300px] pt-10">
                <div className="md:w-1/2  lg:py-20 py-5 lg:px-[12%] md:px-[5%] px-10 space-y-3">
                    <h3 className='text-xl font-semibold pb-5'>Atmosferă și Ambianță</h3>
                    <p>Atmosfera restaurantului este modernă și șic, cu un indiciu al rădăcinilor sale urbane.</p>
                    <p>Spațiul este conceput pentru a crea un mediu confortabil și relaxant pentru cei care servesc masa. Fie că sărbătoriți o ocazie specială sau pur și simplu vă bucurați de o seară în oraș, ambianța restaurantului pregătește terenul pentru o experiență de neuitat.</p>
                </div>
                <div className="md:w-1/2  align-center text-center flex justify-center p-10 rounder-xl relative min-h-[250px] mx-5">
                    <div>
                        <Image src="/urban-kitchen/atmosfera-ambianta.jpg" fill={true} alt="evolutie mancare urban kitchen" className='rounded-xl'></Image>
                    </div>
                </div>
            </div>


        </>
    )
}

export default UrbanKitchen;