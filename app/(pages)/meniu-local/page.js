import { getMenu } from "@/utils/get-menu.js"
import Meniu from '@/components/Menu/Meniu'
import IalocUuid from "@/components/Menu/IalocUuid"
export const metadata = {
  title: "Meniul Nostru | PIATTO 77 - Urban Kitchen ",
  description: 'De la atmosfera caldă și primitoare până la mâncărurile inovatoare și gustoase, fiecare aspect al restaurantului este conceput pentru a oferi o experiență culinară unică.',
  alternates: {
    canonical: 'https://piatto77.ro/meniu',
  },
  languages: "ro-Ro",
  robots: {
    index: false,
    nocache: true
  },
  openGraph: {
    title: "Meniul Nostru | PIATTO 77 - Urban Kitchen ",
    description: 'De la atmosfera caldă și primitoare până la mâncărurile inovatoare și gustoase, fiecare aspect al restaurantului este conceput pentru a oferi o experiență culinară unică.',
    url: 'https://piatto77.ro/meniu',
    siteName: 'PIATTO 77 - Urban Kitchen',
    images: [
      {
        url: '/hero-image.jpg',
        width: 800,
        height: 600,
      },
    ],
    locale: 'ro_RO',
    type: 'website',
  }
}

async function MenuPage() {
  
  const Menu = await getMenu();

  return (
    <main>
      <IalocUuid></IalocUuid>
      <Meniu meniu={Menu}></Meniu>

    </main>
  )
}
export default MenuPage