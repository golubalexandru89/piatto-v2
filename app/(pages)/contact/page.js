import Link from "next/link";
import InstagramIcon from "../../../components/ui/icons/Instagram";
import FacebookIcon from "../../../components/ui/icons/Facebook";
import GoogleMapsLocation from "../../../components/ui/googleMaps/GoogleMaps";

export const metadata = {
    title: "Contact | PIATTO 77 - Urban Kitchen ",
    description: 'Contactați-ne pentru orice întrebare sau ajutor. Suntem aici pentru dumneavoastră în orice moment.',
    languages: "ro-Ro",
    openGraph: {
        title: "Contact | PIATTO 77 - Urban Kitchen ",
        description: 'Contactați-ne pentru orice întrebare sau ajutor. Suntem aici pentru dumneavoastră în orice moment.',
        url: 'https://piatto77.ro/contact',
        siteName: 'PIATTO 77 - Urban Kitchen',
        images: [
            {
                url: '/hero-image.jpg',
                width: 800,
                height: 600,
            },
        ],
        locale: 'ro_RO',
        type: 'website',
    }
}

function Contact() {
    return (
        <>
            <div className=" h-screen w-full flex md:flex-row flex-col">
                <div className="md:w-1/2  flex flex-col justify-center items-center py-10">
                    <div>
                        <h1 className="text-2xl font-semibold">Ne puteți contacta oricând</h1>
                    </div>

                    <div className="align-left text-left space-y-4 pt-5">
                        <Link href="https://goo.gl/maps/iy3SCNaRwmVn4eUM8" className="flex flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z" />
                                <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z" />
                            </svg>
                            <p className="pl-2"> str. Polona 77, București</p>
                        </Link>
                        <Link href="tel:0724554477" className="flex flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 6.75c0 8.284 6.716 15 15 15h2.25a2.25 2.25 0 002.25-2.25v-1.372c0-.516-.351-.966-.852-1.091l-4.423-1.106c-.44-.11-.902.055-1.173.417l-.97 1.293c-.282.376-.769.542-1.21.38a12.035 12.035 0 01-7.143-7.143c-.162-.441.004-.928.38-1.21l1.293-.97c.363-.271.527-.734.417-1.173L6.963 3.102a1.125 1.125 0 00-1.091-.852H4.5A2.25 2.25 0 002.25 4.5v2.25z" />
                            </svg>

                            <p className="pl-2">0724.554.477</p>
                        </Link>
                        <Link href="mailto:contact@piatto77.ro" className="flex flex-row">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M21.75 9v.906a2.25 2.25 0 01-1.183 1.981l-6.478 3.488M2.25 9v.906a2.25 2.25 0 001.183 1.981l6.478 3.488m8.839 2.51l-4.66-2.51m0 0l-1.023-.55a2.25 2.25 0 00-2.134 0l-1.022.55m0 0l-4.661 2.51m16.5 1.615a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V8.844a2.25 2.25 0 011.183-1.98l7.5-4.04a2.25 2.25 0 012.134 0l7.5 4.04a2.25 2.25 0 011.183 1.98V19.5z" />
                            </svg>
                            <p className="pl-2"> contact@piatto77.ro</p>
                        </Link>
                        <Link href="https://www.facebook.com/PIATTO77" className="flex flex-row">
                            <FacebookIcon></FacebookIcon>
                            <p className="pl-2">/ piatto77</p>
                        </Link>
                        <Link href="https://www.instagram.com/piatto.77/" className="flex flex-row">
                            <InstagramIcon></InstagramIcon>
                            <p className="pl-2">@piatto.77</p>
                        </Link>
                    </div>
                </div>
                <div className="md:w-1/2  flex flex-col justify-center items-center relative">
                    <GoogleMapsLocation></GoogleMapsLocation>
                </div>
            </div>
        </>
    )
}
export default Contact;