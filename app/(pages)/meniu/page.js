
import { getMenu } from "@/utils/get-menu.js"
import Meniu from '@/components/Menu/Meniu'


export const metadata = {
  title: "Meniul Nostru | PIATTO 77 - Urban Kitchen ",
  description: 'De la atmosfera caldă și primitoare până la mâncărurile inovatoare și gustoase, fiecare aspect al restaurantului este conceput pentru a oferi o experiență culinară unică.',
  languages: "ro-Ro",
  openGraph: {
    title: "Meniul Nostru | PIATTO 77 - Urban Kitchen ",
    description: 'De la atmosfera caldă și primitoare până la mâncărurile inovatoare și gustoase, fiecare aspect al restaurantului este conceput pentru a oferi o experiență culinară unică.',
    url: 'https://piatto77.ro/meniu',
    siteName: 'PIATTO 77 - Urban Kitchen',
    images: [
      {
        url: '/hero-image.jpg',
        width: 800,
        height: 600,
      },
    ],
    locale: 'ro_RO',
    type: 'website',
  }
}
async function MenuPage() {
  // const parentUrl = document.referrer;
  // console.log("Parent window URL:", parentUrl);

  const Menu = await getMenu();

  return (
    <main>
      <Meniu meniu={Menu}></Meniu>

    </main>
  )
}
export default MenuPage