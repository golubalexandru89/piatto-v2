import Hero from '../components/Hero'
import NewsletterBar from "../components/NewsletterBar"

export const metadata  = {
  title:"PIATTO 77 - Urban Kitchen | Te Simți Ca Acasă ",
  description: 'Vino alături de noi pentru o experiență culinară care întruchipează principiile bucătăriei urbane. Redefinim eleganța și modernitatea bucătăriei urbane.',
  
  languages:"ro-Ro",
  openGraph: {
    title:"Te Simți Ca Acasă | PIATTO 77 - Urban Kitchen",
    description: 'Vino alături de noi pentru o experiență culinară care întruchipează principiile bucătăriei urbane. Redefinim eleganța și modernitatea bucătăriei urbane.',
    url: 'https://piatto77.ro',
    siteName: 'PIATTO 77 - Urban Kitchen',
    images: [
      {
        url: '/hero-image.jpg',
        width: 800,
        height: 600,
      },
    ],
    locale: 'ro_RO',
    type: 'website',
  }
}

async function Home() {



  
  return (
    <main className=" bg-white flex min-h-screen flex-col items-top justify-around">
      <Hero ></Hero>
      <NewsletterBar></NewsletterBar>
    </main>
  )
}
export default Home