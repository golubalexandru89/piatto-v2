export default async function sitemap(){
    const baseUrl = 'https://piatto77.ro';

    return [
        {url:baseUrl, lastModified: new Date()},
        {url:`${baseUrl}/urban-kitchen`, lastModified: new Date()},
        {url:`${baseUrl}/meniu`, lastModified: new Date()},
        {url:`${baseUrl}/evenimente`, lastModified: new Date()},
        {url:`${baseUrl}/contact`, lastModified: new Date()},
        ]
} 