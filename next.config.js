const path = require('path');

module.exports = {
    images:{
        domains:['cloud.gravitylabs.ro','piatto77.ro'],
    },
  webpack: (config) => {
    config.resolve.alias['@'] = path.resolve(__dirname);
    return config;
  },
};
