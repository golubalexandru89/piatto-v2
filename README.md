# Welcome to the Restaurant App. 

The objective of this app is to offer the restaurant owners visibility on the lifetime value of their customers. In order to calculate LTV, the backend of the app finds out the Frequency, Recency and Monetary value of each customer. 

While having this data live, restaurant owners can focus their efforts to personalize their approach, on a customer basis. 

In paralel, it offers the waiters some live data about the expected taste and their visiting patterns, so that they can tailor better services for their customers. 


== Front End ==

The front is quite simple to understand. There are the minimum marketing pages that a restaurant needs to have in order to be able to serve. 

It consists of: 
1. Homepage (/)
2. About the concept(/urban-kitchn)
3. The menu page (/menu)
4. Contact (/contact)

For promotional offers, there are also lead generation pages that are for a specific offer. One example would be events, found at "/evenimente". 


== The Menu == 

The first challenge that's being solved is that of figuring out how many people came to the restaurant and to figure out what marketing campaign was the driving force behind the customer's decision. 

The way to track this, the menu is duplicated for each channel. Ex: the menu that any website visitor has acces is the presentation one (ex: /meniu). For a customer that is inside the restaurant, he will scan a QR code that will send him to a different page (ex: /meniu-local). The menu that somebody who ordered take away and got a promotional flyer is on a different page (ex: /meniu-delivery). 

The menu has the same design and products on all pages, but by being split for each channel, we can see how many people have accessed each page. 


== The Menu - Technical Explanation == 

Because I haven't had access to the POS API, there are scripts that fetch a json with all the product lists from their backend. I've done this by reverse engineering their iframe. 

While it's not the ideal solution, the result is satisfying. This scraper fetches the product list, it then parses and filters the products for each category and then one by one it sends them to a Planetscale database. 

The scripts are found in /app/api/menuEditing and they're being pushed to the DB with Prisma. 
The script that grabs the menu from the DB and sends it for processing is in /utils/get-menu.js



> Now that we have a functional site and the customer can browse products, we are developing the marketing stack in order to do proper campaigns, segmentation and analysis. 


== Pixels and Remarketing == 

There are the marketing pixels that are activated whenever there is a page view of any type. Ex: an user goes to the /menu page and then the /urban-kitchen.

This is being called from the /components/MarketingScripts. 


== Recency and Frequency == 

In order to gather the data that can answer the following questions: "How many times did this customer visit our restaurant and at what time intervals?", we have the following building blocks. 

1. LocalStorage 

To eliminat scaling issues, GDPR issues and any other complications, for start we will be using local storage. Whenever a user visits the website, we store, update and process the session numbers and the restaurant visit numbers. 

In order to do so, the file /components/UserDetails.js is handling these operations. 

2. Context 

We want for the user details to be accessible app wise, so there is a context provider that grabs the user info and stores it. 

This happens in /utils/contextProviders

3. localStorage CRUD  

The scripts that get and process the user info are found /utils/userDetailsFunctions. 
These scripts are at the core of /components/UserDetails.js. 


4. Sending Events

The starting point of a notification system that alerts the waiters about their customers is through Slack webhooks. 
For the moment, the app sends POST requests to /api/slack. 

5. The JSON 
The JSON that is stored in localStorage has the following format: 

`
{
    "puuid": String,
    "first":  Object
        {
        "source": String,
        "medium": String,
        "campaign": String,
        "adset": String,
        "ad": String,
        "campaign_id": String,
        "adset_id": String,
        "ad_id": String,
        "fbclid": String,
        "gclid": String,
        "ttclid": String,
        "cookies": Array
            [
                {
                    "name": String,
                    "value": String
                }
        ],
        "date": Date (dd-mm-yyyy format),
        "hour": time (hh:mm format)
    },
    "last_session_time": (dd-mm-yyyy format),
    "days_since_last_session": Number,
    "last_visit_time": (dd-mm-yyyy format),
    "days_since_last_visit": Number,
    "total_number_of_visits": Number,
    "visits": [
        {
            "visit_number": Number,
            "date": (dd-mm-yyyy format),
            "time": time (hh:mm format)
        }
    ],
    "sessions": [
        {
            "session_number": Number,
            "date": "(dd-mm-yyyy format),
            "time": time (hh:mm format),
            "traffic_details": {
                "source": String,
                "medium": String,
                "campaign": String,
                "adset": String,
                "ad": String,
                "campaign_id": String,
                "adset_id": String,
                "ad_id": String,
                "fbclid": String,
                "gclid": String,
                "ttclid": String,
                "cookies": [
                    {
                        "name": String,
                        "value": String
                    }
                ],
                "date": (dd-mm-yyyy format),
                "hour": time (hh:mm format)
            }
        }
    ]
}
`


=== Menu Category List Order === 
1 Winter Drinks
2 Desert
3 Breakfast
4 Brunch
5 Starter 
6 Salate
7 Pizza
8 Risotto
9 Pasta
10 Burger
11 Peste
12 Meat
13 Side
14 Vinuri la pahar
15 Vinuri la sticla
16 Bubbles
17 Bere
18 Cocktails
19 Gin
20 Coffee
21 Racoritoare
22 Aperitiv Digestiv 
23 Long Drinks
24 Vodka
25 Whisky
26 Cognac & Rum
27 Kids