import {
    Button,
    Tailwind,
    Section,
    Preview,
    Container,
    Heading,
    Text,
    Row,
    Column,
    Link
} from "@react-email/components";
import Head from "next/head";
import * as React from "react";

export default function evenimente(props:any) {
    return (
        <Tailwind
        >
            <Preview>
                Ai primit o cerere de eveniment nouă.
            </Preview>
            <Container key={'container'}>
                <Section key={'first-section'} className="flex flex-col justify-center align-middle items-center bg-gray-50 h-48 text-center p-4">
                    <Heading key={'heading'} as="h1">
                        Cerere de Eveniment
                    </Heading>
                    <Text key={'head-description'} className="font-lead text-lg">Un client a intrat pe pagina de evenimente și a completat cererea. Te rugăm să-l contactezi cât mai curând.</Text>

                </Section>
                <Section key={'second-section'} className="flex flex-col justify-center align-middle items-center bg-red-100 h-48 text-center p-4 font-lead text-xl space-y-4">
                    <Row key={'first-row'} >
                        <Column key={'first-column'}>Nume: {props.name}</Column>
                    </Row>
                    <Row key={'second-row'}>
                        <Column key={'second-column'}>Email: <Link href={`mailto:${props.email}`}>{props.email}</Link></Column>
                    </Row>
                    <Row key={'third-row'}>
                        <Column key={'third-column'}>Telefon: <Link href={`mailto:${props.phone}`}>{props.phone}</Link></Column>
                    </Row>
                </Section>
            </Container>
        </Tailwind>
    );
}
